package jfxsoft.scripts.project_manager;

import org.apache.maven.execution.ProjectDependencyGraph;
import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Plugin;
import org.apache.maven.project.MavenProject;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ComplexMavenProject {

    private final Map<String, ComplexMavenProject> innerProjects = new HashMap<>();
    final Map<String, Set<String>> upstreamDependencies = new HashMap<>();
    final Map<String, Set<String>> downstreamDependencies = new HashMap<>();
    private final Set<String> innerDependencies = new HashSet<>();
    final MavenProject mavenProject;
    final boolean isRoot;

    public ComplexMavenProject(MavenProject mavenProject, boolean isRoot) {
        this.mavenProject = mavenProject;
        this.isRoot = isRoot;
        innerDependencies.addAll(extractModelDependencies(mavenProject));
        innerDependencies.addAll(extractModelPluginDependencies(mavenProject));
    }

    final Map<String, ComplexMavenProject> loadInnerDependencies() throws Exception {
        final Path projectDirectory = mavenProject.getModel().getPomFile().getParentFile().toPath();
        final Map<String, ComplexMavenProject> innerProjects = PomReader.readProjectsInDirectory(projectDirectory, childProject -> new ComplexMavenProject(childProject, false), Collectors.toMap(prj -> prj.mavenProject.getArtifactId(), prj -> prj), Integer.MAX_VALUE);
        this.innerProjects.putAll(innerProjects);
        for (ComplexMavenProject innerProject : innerProjects.values()) {
            innerProjects.putAll(innerProject.loadInnerDependencies());
            innerDependencies.remove(innerProject.mavenProject.getArtifactId());
        }

        return innerProjects;
    }

    final Set<String> getInnerDependnecies() {
        final Set<String> innerDependencies = new HashSet<>(this.innerDependencies);

        innerDependencies.addAll(innerProjects.values().stream().map(ComplexMavenProject::getInnerDependnecies).flatMap(Collection::stream).filter(artifactId -> !innerProjects.containsKey(artifactId)).collect(Collectors.toSet()));

        return innerDependencies;
    }

    final void cleanupInnerDependencies(Set<String> artifacts) {
        innerDependencies.removeIf(dependencyArtifact -> !artifacts.contains(dependencyArtifact) || innerProjects.containsKey(dependencyArtifact));

        innerProjects.values().forEach(project -> project.cleanupInnerDependencies(artifacts));

    }

    final Set<String> getInnerProjects() {
        return innerProjects.values().stream().map(project -> project.mavenProject.getArtifactId()).collect(Collectors.toSet());
    }

    void resolveDependencies(ProjectDependencyGraph projectDependencyGraph, final Map<String, ComplexMavenProject> allProjects, Set<String> processedProjects) throws Exception {
        resolveDependencies(mavenProject, projectDependencyGraph, upstreamDependencies, downstreamDependencies);

        upstreamDependencies.getOrDefault(mavenProject.getArtifactId(), Collections.emptySet()).remove(mavenProject.getArtifactId());
        downstreamDependencies.getOrDefault(mavenProject.getArtifactId(), Collections.emptySet()).remove(mavenProject.getArtifactId());

        processInnerDependencies(projectDependencyGraph, allProjects, processedProjects, upstreamDependencies);
        processInnerDependencies(projectDependencyGraph, allProjects, processedProjects, downstreamDependencies);
    }

    private static void processInnerDependencies(ProjectDependencyGraph projectDependencyGraph, Map<String, ComplexMavenProject> allProjects, Set<String> processedProjects, Map<String, Set<String>> upstreamDependencies) throws Exception {
        for (Map.Entry<String, Set<String>> entry : upstreamDependencies.entrySet()) {
            if (processedProjects.contains(entry.getKey())) { continue; }
            processedProjects.add(entry.getKey());
            final ComplexMavenProject innerComplexMavenProject = allProjects.get(entry.getKey());
            innerComplexMavenProject.resolveDependencies(projectDependencyGraph, allProjects, processedProjects);
        }
    }

    static void resolveDependencies(MavenProject mavenProject, ProjectDependencyGraph dependencyGraph, Map<String, Set<String>> upstreamDependencies, Map<String, Set<String>> downstreamDependencies) {
        resolveUpstreamDependencies(mavenProject, dependencyGraph, upstreamDependencies);
        resolveDownstreamDependencies(mavenProject, dependencyGraph, upstreamDependencies, downstreamDependencies);
    }

    static void resolveUpstreamDependencies(MavenProject mavenProject, ProjectDependencyGraph dependencyGraph, Map<String, Set<String>> upstreamDependencies) {
        final List<MavenProject> upstreamProjects = dependencyGraph.getUpstreamProjects(mavenProject, false);

        for (MavenProject upstreamProject : upstreamProjects) {
            upstreamDependencies.computeIfAbsent(mavenProject.getArtifactId(), s -> new HashSet<>()).add(upstreamProject.getArtifactId());
        }
    }

    static void resolveDownstreamDependencies(MavenProject mavenProject, ProjectDependencyGraph dependencyGraph, Map<String, Set<String>> upstreamDependencies, Map<String, Set<String>> downstreamDependencies) {
        final List<MavenProject> downStreamProjects = dependencyGraph.getDownstreamProjects(mavenProject, false);

        for (MavenProject downstreamProject : downStreamProjects) {
            downstreamDependencies.computeIfAbsent(mavenProject.getArtifactId(), s -> new HashSet<>()).add(downstreamProject.getArtifactId());

            resolveDependencies(downstreamProject, dependencyGraph, upstreamDependencies, downstreamDependencies);
        }
    }

    static Set<String> extractModelDependencies(MavenProject mavenProject) {
        return extractModelDependencies(mavenProject.getModel().getDependencies());
    }

    static Set<String> extractModelDependencies(List<Dependency> dependencies) {
        return dependencies.stream().map(Dependency::getArtifactId).collect(Collectors.toSet());
    }

    static Set<String> extractModelPluginDependencies(MavenProject mavenProject) {
        final List<Plugin> plugins = Optional.ofNullable(mavenProject.getModel().getBuild()).orElse(new Build()).getPlugins();
        final Set<String> artifacts = new HashSet<>();

        for (Plugin plugin : plugins) {
            artifacts.add(plugin.getArtifactId());
            artifacts.addAll(extractModelDependencies(plugin.getDependencies()));
        }

        return artifacts;
    }
}
