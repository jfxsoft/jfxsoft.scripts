package jfxsoft.scripts.project_manager;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;

public class PomReader {

    static MavenXpp3Reader mavenReader = new MavenXpp3Reader();

    static <T> Map<String, T> readProjectsInDirectory(Path projectRootDirectory, Function<MavenProject, T> projectConverter, Collector<T, ?, Map<String, T>> collector, int depth) throws IOException {
        return Files.walk(projectRootDirectory, depth)
                .filter(Files::isDirectory)
                .filter(path -> !path.toString().contains("target"))
                .filter(path -> !projectRootDirectory.equals(path))
                .filter(path -> path.getFileName().toString().startsWith("jfxsoft"))
                .map(path -> path.resolve("pom.xml"))
                .filter(Files::exists)
                .map(path -> projectConverter.apply(readPom(path.toFile())))
                .collect(collector);
    }

    static MavenProject readPom(File pomfile) {
        Model model = null;
        FileReader reader = null;
        try {
            reader = new FileReader(pomfile);
            model = mavenReader.read(reader);
            model.setPomFile(pomfile);
        } catch(Exception ex){}

        return new MavenProject(model);
    }

}
