package jfxsoft.scripts.project_manager;

import org.apache.maven.execution.ProjectDependencyGraph;
import org.apache.maven.graph.DefaultProjectDependencyGraph;
import org.apache.maven.project.MavenProject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static jfxsoft.scripts.project_manager.PomReader.readProjectsInDirectory;

public class Application {

    static ExecutorService executorService = Executors.newSingleThreadExecutor();

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage: java -jar application path/to/root/directory");
            return;
        }

        final Path projectRootDirectory = Paths.get(args[0]);
        final Map<String, ComplexMavenProject> mavenProjects = readProjectsInDirectory(projectRootDirectory, mavenProject -> new ComplexMavenProject(mavenProject, true), Collectors.toMap(prj -> prj.mavenProject.getArtifactId(), prj -> prj), 1);
        final Map<String, ComplexMavenProject> allProjects = new HashMap<>(mavenProjects);
        for (ComplexMavenProject value : mavenProjects.values()) {
            allProjects.putAll(value.loadInnerDependencies());
        }

        final ProjectDependencyGraph dependencyGraph = new DefaultProjectDependencyGraph(allProjects.values().stream().map(prj -> prj.mavenProject).toList());
        final ComplexMavenProject parentMavenProject = mavenProjects.get("jfxsoft.parent");

        {
            final Set<String> processedProjects = new HashSet<>();
            parentMavenProject.resolveDependencies(dependencyGraph, allProjects, processedProjects);
            mavenProjects.values().forEach(project -> project.cleanupInnerDependencies(allProjects.keySet()));
        }

        final Deque<String> artifactStack = new LinkedList<>();
        final Set<String> artifactStackHelper = new HashSet<>();
        final Set<String> alreadyProcessed = new LinkedHashSet<>();
        final Set<String> projectsInOrder = new LinkedHashSet<>();
        projectsInOrder.add(parentMavenProject.mavenProject.getArtifactId());
        artifactStack.add(parentMavenProject.mavenProject.getArtifactId());
        artifactStackHelper.add(parentMavenProject.mavenProject.getArtifactId());

        processProjects(artifactStack, allProjects, parentMavenProject, alreadyProcessed, mavenProjects, artifactStackHelper, projectsInOrder);

        for (String s : projectsInOrder) {
            final int retCode = exec(mavenProjects.get(s).mavenProject, "/home/petr/.local/share/JetBrains/Toolbox/apps/intellij-idea-ultimate/plugins/maven/lib/maven3/bin/mvn", "clean", "install", "-DskipTests=true"/*, "-P", "gluon,dagger,executable,guice"*/);
            if (retCode != 0) {
                System.out.println("Fail!");
                break;
            }
            System.out.println("================================================================");
        }

        executorService.shutdown();
    }

    private static void processProjects(Deque<String> artifactStack, Map<String, ComplexMavenProject> allProjects, ComplexMavenProject parentMavenProject, Set<String> alreadyProcessed, Map<String, ComplexMavenProject> mavenProjects, Set<String> artifactStackHelper, Set<String> projectsInOrder) {
        while (!artifactStack.isEmpty()) {
            System.out.println(artifactStack);
            final String currentArtifactId = artifactStack.pop();
            final ComplexMavenProject complexMavenProject = allProjects.get(currentArtifactId);

            final Set<String> upstreamDependencies = parentMavenProject.upstreamDependencies.getOrDefault(currentArtifactId, Collections.emptySet());
            final Set<String> dependencies = complexMavenProject.getInnerDependnecies();
            upstreamDependencies.addAll(dependencies);
            if (!alreadyProcessed.containsAll(upstreamDependencies)) {
                artifactStack.add(currentArtifactId);
                continue;
            }

            final Set<String> downstreamDependencies = new HashSet<>(parentMavenProject.downstreamDependencies.getOrDefault(currentArtifactId, Collections.emptySet()));
            final Set<String> innerProjects = complexMavenProject.getInnerProjects();
            processArtifact(alreadyProcessed, downstreamDependencies, artifactStack, mavenProjects, artifactStackHelper, currentArtifactId, projectsInOrder, innerProjects);
        }
    }

    private static void processArtifact(Set<String> alreadyProcessed, Set<String> downstreamDependencies, Deque<String> artifactStack, Map<String, ComplexMavenProject> mavenProjects, Set<String> artifactStackHelper, String currentArtifactId, Set<String> projectsInOrder, Set<String> innerProjects) {
        if (!alreadyProcessed.containsAll(downstreamDependencies)) {
            downstreamDependencies.removeAll(alreadyProcessed);
            if (!artifactStack.containsAll(downstreamDependencies)) {
                artifactStack.forEach(downstreamDependencies::remove);
                checkDownstreamDependencies(downstreamDependencies, alreadyProcessed, mavenProjects, artifactStackHelper, artifactStack);
                artifactStack.addFirst(currentArtifactId);
            } else {
                markArtifactAsProcessed(alreadyProcessed, currentArtifactId, mavenProjects, projectsInOrder, innerProjects, artifactStack, artifactStackHelper);
            }
        } else {
            markArtifactAsProcessed(alreadyProcessed, currentArtifactId, mavenProjects, projectsInOrder, innerProjects, artifactStack, artifactStackHelper);
        }
    }

    private static void checkDownstreamDependencies(Set<String> downstreamDependencies, Set<String> alreadyProcessed, Map<String, ComplexMavenProject> mavenProjects, Set<String> artifactStackHelper, Deque<String> artifactStack) {
        for (String downstreamDependency : downstreamDependencies) {
            if (!alreadyProcessed.contains(downstreamDependency)) {
                final ComplexMavenProject downstreamProject = mavenProjects.get(downstreamDependency);
                if (downstreamProject != null) {
                    for (String s : downstreamProject.getInnerProjects()) {
                        if (artifactStackHelper.add(s)) {
                            artifactStack.add(s);
                        }
                    }
                }
                if (artifactStackHelper.add(downstreamDependency)) {
                    artifactStack.addFirst(downstreamDependency);
                }
            }
        }
    }

    private static void markArtifactAsProcessed(Set<String> alreadyProcessed, String currentArtifactId, Map<String, ComplexMavenProject> mavenProjects, Set<String> projectsInOrder, Set<String> innerProjects, Deque<String> artifactStack, Set<String> artifactStackHelper) {
        alreadyProcessed.add(currentArtifactId);
        if (mavenProjects.containsKey(currentArtifactId)) {
            projectsInOrder.add(currentArtifactId);
        }

        alreadyProcessed.addAll(innerProjects);
        artifactStack.removeAll(innerProjects);
        artifactStackHelper.removeAll(innerProjects);
        artifactStackHelper.remove(currentArtifactId);
    }

    static int exec(MavenProject mavenProject, String ...commands) throws Exception {
        final ProcessBuilder mvndCleanInstall = new ProcessBuilder(commands)
                .directory(mavenProject.getModel().getPomFile().getParentFile())
                .inheritIO(); // make sure that spawned process will share IO with parent -> automatic logging
        final Process process = mvndCleanInstall.start();

        return process.waitFor();
    }

}
