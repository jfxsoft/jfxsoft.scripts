import { MergeRequestResponse, Project } from '../types';

export interface GitlabMergeRequester {

    autoMergeRequest(mergeRequest: MergeRequestResponse): Promise<boolean>;

    createMergeRequest(project: Project, sourceBranch: string, targetBranch: string, title: string): Promise<boolean | MergeRequestResponse>;
}
