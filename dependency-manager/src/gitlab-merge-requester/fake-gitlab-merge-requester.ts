import { ParameterValueProvider } from '../text-renderer';
import { MergeRequestResponse, Project } from '../types';

import { GitlabMergeRequester } from './gitlab-merge-requester';

export class FakeGitlabMergeRequester implements GitlabMergeRequester {

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	initMessages(parameters: ParameterValueProvider[]): void {
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	autoMergeRequest(mergeRequest: MergeRequestResponse): Promise<boolean> {
		return Promise.resolve(true);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	createMergeRequest(project: Project, sourceBranch: string, targetBranch: string, title: string): Promise<boolean | MergeRequestResponse> {
		return Promise.resolve({
			id: 0,
			iid: 0,
			project_id: 0,
			title: '',
			web_url: '',
			author: {
				id: 0,
				name: '',
				username: '',
				state: '',
				avatar_url: '',
				web_url: '',
			},
		});
	}


}
