import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { KeyParameterValueProvider } from '../key-parameter-value-provider';
import { getLogger, Logger } from '../logger';
import { ParameterValueProvider, ParametrizedTextRenderer } from '../text-renderer';
import { AutoMergeRequest, MergeRequest, MergeRequestResponse, Project, Projects } from '../types';

import { GitlabMergeRequester } from './gitlab-merge-requester';

export class RealGitlabMergeRequester implements GitlabMergeRequester {

	private static logger: Logger = getLogger('RealGitlabMergeRequester');

	protected readonly storeResponse: boolean;

	public constructor(protected readonly envProvider: EnvironmentProvider,
                       protected readonly httpClient: HttpsClient,
                       protected readonly projects: Projects,
                       protected readonly parameters: ParameterValueProvider[]) {
		this.storeResponse = envProvider.getSTORE_RESPONSES();
		this.parameters.push(KeyParameterValueProvider.forKey('merge-request'));
	}

	public async autoMergeRequest(mergeRequest: MergeRequestResponse): Promise<boolean> {
		RealGitlabMergeRequester.logger.info(`Auto-complete merge request ${mergeRequest.iid} of project ${mergeRequest.project_id} when pipelines finish successfully.`);

		const commitMessage: string | undefined = this.envProvider.getCOMMIT_MESSAGE();
		const squashMessage: string | undefined = this.envProvider.getSQUASH_MESSAGE();
		const params: { [key: string]: unknown } = {};
		params['merge-request'] = mergeRequest;
		params['project'] = this.projects.find(project => project.id === mergeRequest.project_id);

		const autoMergeRequestPayload: AutoMergeRequest = {
			merge_commit_message: commitMessage ? new ParametrizedTextRenderer(commitMessage).render(this.parameters, params) : undefined,
			merge_when_pipeline_succeeds: true,
			should_remove_source_branch: true,
			squash_commit_message: squashMessage ? new ParametrizedTextRenderer(squashMessage).render(this.parameters, params) : undefined,
			squash: true
		};

		return await this.httpClient.put(this.createAutoMergeRequestUrl(mergeRequest.project_id, mergeRequest.iid), autoMergeRequestPayload);
	}

	public async createMergeRequest(project: Project, sourceBranch: string, targetBranch: string, title: string): Promise<boolean | MergeRequestResponse> {
		RealGitlabMergeRequester.logger.info(`Creating new merge request for project: ${project.name} from branch: ${sourceBranch} to ${targetBranch} with title: ${title}.`);

		const mergeRequestPayload: MergeRequest = {
			source_branch: sourceBranch,
			target_branch: targetBranch,
			title
		};

		RealGitlabMergeRequester.logger.info(mergeRequestPayload);
		return await this.httpClient.post(this.createMergeRequestUrl(project.id), mergeRequestPayload);
	}

	protected createAutoMergeRequestUrl(projectId: number, mergeRequestId: number) {
		return `projects/${projectId}/merge_requests/${mergeRequestId}/merge`;
	}

	protected createMergeRequestUrl(projectId: number) {
		return `projects/${projectId}/merge_requests`;
	}

}
