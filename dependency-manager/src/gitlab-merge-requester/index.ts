import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';
import { ParameterValueProvider } from '../text-renderer';
import { Projects } from '../types';

import { RealGitlabMergeRequester } from './real-gitlab-merge-requester';

const logger: Logger = getLogger('GitlabMergeRequestsResolver');

export function getGitlabMergeRequesterInstance(envProvider: EnvironmentProvider, httpClient: HttpsClient, projects: Projects, parameters: ParameterValueProvider[]) {
	// if (envProvider.isCI()) {
	logger.info('Using RealGitlabMergeRequester...');
	return new RealGitlabMergeRequester(envProvider, httpClient, projects, parameters);
	// }

	// logger.info('Using FakeGitlabMergeRequester...');
	// return new FakeGitlabMergeRequester();
}

export * from './gitlab-merge-requester';
