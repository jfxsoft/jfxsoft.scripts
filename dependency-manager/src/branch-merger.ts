import { EnvironmentProvider } from './environment-provider';
import { getGitlabFetcherInstance, GitlabFetcher } from './gitlab-fetcher';
import { getGitlabMergeRequesterInstance, GitlabMergeRequester } from './gitlab-merge-requester';
import { HttpsClient } from './https-client';
import { getLogger, Logger } from './logger';
import { MergeRequestResponse, Projects } from './types';

const logger: Logger = getLogger('main');

async function main() {
	const envProvider: EnvironmentProvider = new EnvironmentProvider();
	const httpClient: HttpsClient = new HttpsClient(envProvider);
	const gitlabFetcher: GitlabFetcher = getGitlabFetcherInstance(envProvider, httpClient);

	const mainGroup = await gitlabFetcher.fetchMainGroup();
	if (!mainGroup) {
		logger.error('Could not obtain main group of the project...');
		return;
	}
	logger.info(`Main group id: ${mainGroup.id}, name: '${mainGroup.name}'.`);

	const projects = await gitlabFetcher.fetchProjects(mainGroup);
	if (!projects) {
		logger.error('Could not obtain projects in main group...')
		return;
	}
	logger.info(`Loaded #${projects.length} projects.`);

	await createPullRequests(envProvider, httpClient, projects);
}

async function createPullRequests(envProvider: EnvironmentProvider, httpClient: HttpsClient, projects: Projects) {
	const mergeRequester: GitlabMergeRequester = getGitlabMergeRequesterInstance(envProvider, httpClient, projects, []);

	const prj = new Set([
		'jfxsoft.annotations.parent',
		'jfxsoft.archetype.parent',
		'jfxsoft.easymvc.aggregator',
		'jfxsoft.easymvc.api.parent',
		'jfxsoft.easymvc.archetype.application',
		'jfxsoft.easymvc.archetype.procedure',
		'jfxsoft.easymvc.parent',
		'jfxsoft.maven-plugin-extension',
		'jfxsoft.maven-plugin-extension.config-addon',
		'jfxsoft.maven-plugin-extension.parent',
		'jfxsoft.maven-plugin-extension.po-generator',
		'jfxsoft.maven-plugin-extension.properties-addon',
		'jfxsoft.maven-plugin.ast',
		'jfxsoft.maven-plugin.external-project-fetcher',
		'jfxsoft.maven-plugin.external-project-fetcher.api',
		'jfxsoft.maven-plugin.external-project-fetcher.gitlab-fetcher',
		'jfxsoft.maven-plugin.external-project-fetcher.parent',
		'jfxsoft.maven-plugin.javassist',
		'jfxsoft.maven-plugin.parent',
		'jfxsoft.maven-plugin.resources',
		'jfxsoft.persistency.maven-plugin',
		'jfxsoft.persistency.parent',
		'jfxsoft.platform-extension.parent',
		'jfxsoft.platform.parent',
		'jfxsoft.redux.parent'
	]);

	for (const project of projects) {
		if (prj.has(project.name)) {
			const mrResponse: boolean|MergeRequestResponse = await mergeRequester.createMergeRequest(project, 'develop', 'main', 'Merge develop branch');
			if (!mrResponse) {
				logger.error('MR could not be created!');
				continue;
			}
		}
	}
}

main().catch((reason: Error) => {
	logger.error(reason.message);
	logger.error(reason.stack);
})
