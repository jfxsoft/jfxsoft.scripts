import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';

import { FakeGitlabFetcher } from './fake-gitlab-fetcher';
import { RealGitlabFetcher } from './real-gitlab-fetcher';

const logger: Logger = getLogger('GitlabFetcherResolver');

export function getGitlabFetcherInstance(envProvider: EnvironmentProvider, httpClient: HttpsClient) {
	if (envProvider.isCI()) {
		logger.info('Using RealGitlabFetcher...');
		return new RealGitlabFetcher(envProvider, httpClient);
	}

	logger.info('Using FakeGitlabFetcher...');
	return new FakeGitlabFetcher();
}

export * from './gitlab-fetcher';