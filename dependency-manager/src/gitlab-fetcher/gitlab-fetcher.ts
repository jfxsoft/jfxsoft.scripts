import { FileInformation, Group, Projects, TreeFileInfo } from '../types';

export function buildShorterFileName(partialNames: string[]): string {
	const partialNamesCopy = [...partialNames];
	const fullLastName: string | undefined = partialNamesCopy.pop();
	let nameBuffer = '';
	if (fullLastName) {
		nameBuffer = fullLastName;
		if (partialNamesCopy.length !== 0) {
			do {
				const parentPartialName = partialNamesCopy.pop();
				if (!parentPartialName) {
					break;
				}
				const shortParentPartialName = parentPartialName.split('-').map(value => value.charAt(0)).join('');
				nameBuffer = shortParentPartialName + '_' + nameBuffer;

			} while (partialNamesCopy.length !== 0);
			nameBuffer = '_' + nameBuffer;
		} else {
			nameBuffer = '_' + nameBuffer;
		}
	}

	return nameBuffer;
}

export interface GitlabFetcher {

	fetchMainGroup(): Promise<Group | false>;

	fetchProjects(group: Group): Promise<Projects | false>;

	fetchPom(projectId: number, branchName: string): Promise<FileInformation | false>;
	fetchPom(projectId: number, branchName: string, innerPomPath: string): Promise<FileInformation | false>;

	fetchChanges(projectId: number, branchName: string, changesFileName: string): Promise<FileInformation | false>;
	fetchChanges(projectId: number, branchName: string, changesFileName: string, innerChangesPath: string): Promise<FileInformation | false>;

	fetchDocsFilesInfo(projectId: number, branchName: string): Promise<TreeFileInfo[] | false>;
	fetchDocsFilesInfo(projectId: number, branchName: string, innerDocsPath: string[]): Promise<TreeFileInfo[] | false>;

	fetchDocFile(projectId: number, branchName: string, docPath: string): Promise<FileInformation | false>

    fetchModuleInfoFileInfo(projectId: number, branchName: string, innerProjectPath: string[]): Promise<FileInformation | false>;
}
