import fs from 'fs';
import path from 'path';

import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';
import { FileInformation, Group, Projects, TreeFileInfo, TreeStructure } from '../types';

import { buildShorterFileName, GitlabFetcher } from './gitlab-fetcher';

export class RealGitlabFetcher implements GitlabFetcher {

	private static logger: Logger = getLogger('RealGitlabFetcher');

	protected readonly mainGroup: string;
	protected readonly storeResponse: boolean;

	public constructor(protected readonly envProvider: EnvironmentProvider,
		protected readonly httpClient: HttpsClient) {
		this.mainGroup = `groups/${envProvider.getGROUP_NAME()}`;
		this.storeResponse = envProvider.getSTORE_RESPONSES();
	}

	public async fetchMainGroup(): Promise<Group | false> {
		const response = await this.httpClient.get<Group>(this.createMainGroupRequestUrl());
		if (this.storeResponse) {
			await this.writeFile('runtime/group.json', response);
		}

		return response;
	}

	public async fetchProjects(group: Group): Promise<Projects | false> {
		const tree: TreeStructure = {};
		const response = await this.fetchProjectsRecursive(group, tree);

		await this.writeFile('runtime/projects.json', response);
		await this.writeFile('runtime/project-tree.json', tree);

		return response;
	}

	public async fetchPom(projectId: number, branchName: string, innerPomPath: string = ''): Promise<FileInformation | false> {
		const response = await this.httpClient.get<FileInformation>(this.createPomRequestUrl(projectId, branchName, innerPomPath));
		if (this.storeResponse && response) {
			await this.writeFile(`runtime/${projectId}${innerPomPath.length !== 0 ? '_' : ''}${innerPomPath.replaceAll('/', '_')}.json`, response);
		}
		return response;
	}

	public async fetchChanges(projectId: number, branchName: string, changesFileName: string, innerChangesPath: string = ''): Promise<FileInformation | false> {
		const response = await this.httpClient.get<FileInformation>(this.createChangesRequestUrl(projectId, branchName, changesFileName, innerChangesPath));
		if (response) {
			await this.writeFile(`runtime/${projectId}${innerChangesPath.length !== 0 ? '_' : ''}${innerChangesPath.replaceAll('/', '_')}_${changesFileName}`, response);
		}
		return response;
	}

	public async fetchDocsFilesInfo(projectId: number, branchName: string, innerDocsPath: string[] = []): Promise<TreeFileInfo[] | false> {
		const response = await this.fetchDocsFilesInfoRecursive(projectId, branchName, `${innerDocsPath.join('/')}${innerDocsPath.length !== 0 ? '/' : ''}docs`);
		if (this.storeResponse && response) {
			await this.writeFile(`runtime/${projectId}${buildShorterFileName(innerDocsPath)}_docs.json`, response);
		}

		return response;
	}

	public async fetchDocFile(projectId: number, branchName: string, docPath: string): Promise<FileInformation | false> {
		const response = await this.httpClient.get<FileInformation>(this.createFileRequestUrl(projectId, branchName, docPath));
		if (response) {
			const filePath = `runtime/${projectId}/${(<FileInformation>response).file_path}`.replace('/docs', '');
			const fileName = `${(<FileInformation>response).file_name}`;
			const dirPath = filePath.substring(0, filePath.indexOf(fileName));
			await fs.promises.mkdir(dirPath, { recursive: true });
			await fs.promises.writeFile(filePath, Buffer.from(response.content, 'base64').toString('utf-8'), { encoding: 'utf-8', flag: 'w' });
		}

		return response;
	}

	public async fetchModuleInfoFileInfo(projectId: number, branchName: string, innerProjectPath: string[] = []): Promise<FileInformation | false> {
		const response = await this.httpClient.get<FileInformation>(this.createFileRequestUrl(projectId, branchName, [innerProjectPath.join('/'), 'src', 'main', 'java', 'module-info.java'].join('/')));

		if (response) {
			await this.writeFile(`runtime/${projectId}/${buildShorterFileName(innerProjectPath)}_module_info.json`, response);
		}

		return response;
	}



	protected async fetchDocsFilesInfoRecursive(projectId: number, branchName: string, docsPath: string): Promise<TreeFileInfo[] | false> {
		const response = await this.httpClient.get<TreeFileInfo[]>(this.createFileTreeRequestUrl(projectId, branchName, docsPath));
		if (!response) {
			return false;
		}

		const files: TreeFileInfo[] = [];

		for (const treeFileInfo of response) {
			if (treeFileInfo.type === 'tree') {
				const innerResponse = await this.fetchDocsFilesInfoRecursive(projectId, branchName, treeFileInfo.path);
				if (innerResponse) {
					files.push(...(<TreeFileInfo[]>innerResponse));
				}
			} else {
				files.push(treeFileInfo);
			}
		}

		return files;
	}

	protected async fetchProjectsRecursive(group: Group, tree: TreeStructure): Promise<Projects> {
		const projects: Projects = [];
		const parentTree: TreeStructure = {};
		tree[group.name] = parentTree;

		if (group.projects) {
			for (const project of group.projects) {
				projects.push(project);
				parentTree[project.name] = project.id;
			}
		} else {
			const fetchedProjects: Projects | false = await this.httpClient.get(this.createProjectsInGroupRequestUrl(group.id));
			if (fetchedProjects) {
				for (const project of fetchedProjects) {
					projects.push(project);
					parentTree[project.name] = project.id;
				}
			}
		}

		const subgroups: Group[] | false = await this.httpClient.get(this.createSubgroupsInGroupRequestUrl(group.id));
		if (subgroups) {
			for (const subgroup of subgroups) {
				const subTree: TreeStructure = {};
				const subprojects: Projects = await this.fetchProjectsRecursive(subgroup, subTree);
				parentTree[subgroup.name] = subTree[subgroup.name];
				projects.push(...subprojects);
			}
		}

		return projects;
	}

	protected async writeFile(filePath: string, content: unknown) {
		RealGitlabFetcher.logger.info(`Writing content to file: ${path.resolve(filePath)}`);
		await fs.promises.writeFile(filePath, JSON.stringify(content), { encoding: 'utf-8', flag: 'w' });
	}

	protected createMainGroupRequestUrl() {
		return this.mainGroup;
	}

	protected createProjectsInGroupRequestUrl(groupId: number) {
		return `groups/${groupId}/projects`;
	}

	protected createSubgroupsInGroupRequestUrl(groupId: number) {
		return `groups/${groupId}/subgroups`;
	}

	protected createFileRequestUrl(projectId: number, branchName: string, filePath: string) {
		filePath = filePath.replaceAll('/', '%2F');
		return `projects/${projectId}/repository/files/${filePath}?ref=${branchName}`;
	}

	protected createPomRequestUrl(projectId: number, branchName: string, innerPomPath: string) {
		return this.createFileRequestUrl(projectId, branchName, `${innerPomPath}${innerPomPath.length !== 0 ? '/' : ''}pom.xml`);
	}

	protected createChangesRequestUrl(projectId: number, branchName: string, changesFileName: string, innerChangesPath: string) {
		return this.createFileRequestUrl(projectId, branchName, `${innerChangesPath}${innerChangesPath.length !== 0 ? '/' : ''}${changesFileName}`);
	}

	protected createFileTreeRequestUrl(projectId: number, branchName: string, path: string = '') {
		let url = `projects/${projectId}/repository/tree?ref=${branchName}`;

		if (path && path.trim().length != 0) {
			url += `&path=${path}`;
		}

		return url;
	}

}
