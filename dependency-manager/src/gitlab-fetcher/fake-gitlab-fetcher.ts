import fs from 'fs';

import { getLogger, Logger } from '../logger';
import { FileInformation, Group, Projects, TreeFileInfo } from '../types';

import { buildShorterFileName, GitlabFetcher } from './gitlab-fetcher';

export class FakeGitlabFetcher implements GitlabFetcher {

	protected readonly logger: Logger = getLogger(FakeGitlabFetcher.name);

	public async fetchMainGroup(): Promise<Group | false> {
		this.logger.info('Reading main group...');
		return this.parseJson(await this.readFile('runtime/group.json'));
	}

	public async fetchProjects(): Promise<Projects | false> {
		this.logger.info('Reading all projects...');
		return this.parseJson(await this.readFile('runtime/projects.json'));
	}

	public async fetchPom(projectId: number, branchName: string, innerPomPath: string = ''): Promise<FileInformation | false> {
		this.logger.info(`Reading pom.xml for project id: ${projectId}...`);
		const filePath = `runtime/${projectId}${innerPomPath.length !== 0 ? '_' : ''}${innerPomPath.replaceAll('/', '_')}.json`;
		if (!fs.existsSync(filePath)) return false;
		return this.parseJson(await this.readFile(filePath));
	}

	public async fetchChanges(projectId: number, branchName: string, changesFileName: string): Promise<FileInformation | false> {
		this.logger.info(`Reading ${changesFileName} for project id: ${projectId}...`);
		if (!fs.existsSync(`runtime/${projectId}_${changesFileName}`)) return false;
		return this.parseJson(await this.readFile(`runtime/${projectId}_${changesFileName}`));
	}

	public async fetchDocsFilesInfo(projectId: number, branchName: string, innerDocsPath: string[] = []): Promise<TreeFileInfo[] | false> {
		this.logger.info(`Reading list of documentation files for project id: ${projectId}...`);
		const filePath = `runtime/${projectId}${buildShorterFileName(innerDocsPath)}_docs.json`;
		if (!fs.existsSync(filePath)) return false;
		return this.parseJson(await this.readFile(filePath));
	}

	public async fetchDocFile(projectId: number, branchName: string, docPath: string): Promise<FileInformation | false> {
		docPath = docPath.replace(/\/?docs/, '');
		this.logger.info(`Reading documentation file in project: ${projectId} from: ${docPath}`);
		if (!fs.existsSync(`runtime/${projectId}/${docPath}.json`)) return false;
		return this.parseJson(await this.readFile(`runtime/${projectId}/${docPath}.json`));
	}

	public async fetchModuleInfoFileInfo(projectId: number, branchName: string, innerProjectPath: string[]): Promise<FileInformation | false> {
		this.logger.info(`Reading list of documentation files for project id: ${projectId}...`);
		const filePath = `runtime/${projectId}/${buildShorterFileName(innerProjectPath)}_module_info.json`;
		if (!fs.existsSync(filePath)) return false;
		return this.parseJson(await this.readFile(filePath));
	}


	protected async readFile(filePath: string): Promise<string> {
		return await fs.promises.readFile(filePath, { encoding: 'utf-8' });
	}

	protected parseJson<T>(content: string): T {
		return JSON.parse(content);
	}
}
