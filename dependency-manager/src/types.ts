import { ArtifactVersion } from './artifact-version';

export interface Project {
	id: number;
	name: string;
	ssh_url_to_repo: string;
	_links: {
		self: string;
		issues: string;
		merge_requests: string;
		repo_branches: string;
		labels: string;
		events: string;
		members: string;
		cluster_agents: string;
	}
	default_branch: string;
	scm: {
		connection?: string;
		developerConnection?: string;
		tag: string;
	}
	// custom fields
	pom: string;
}

export type Projects = Project[];

export interface Group {
	id: number;
	name: string;
	web_url: string;
	projects: Projects;
}

export interface FileInformation {
	file_name: string;
	file_path: string;
	size: number;
	encoding: BufferEncoding;
	content: string;
	content_sha256: string;
	ref: string;
	blob_id: string;
	commit_id: string;
	last_commit_id: string;
	execute_filemode: boolean;
}

export type PomProperrties = { [key: string]: string };

export interface POM {
	project: {
		modelVersion: string;
		parent?: {
			groupId: string;
			artifactId: string;
			version: string;
			relativePath?: string;
		}

		groupId?: string;
		artifactId: string;
		version?: string;
		packaging?: string;

		distributionManagement: [{
			repository: {
				id: string;
				url: string;
			}
		}],

		properties?: PomProperrties,

		repositories?: {
			repository: [{
				id: string;
				url: string;
			}] | {
				id: string;
				url: string;
			}
		},

		modules?: {
			module: string | string[]
		}
	}
}

export type DependencyGraph = Map<string, ArtifactVersionInformation>;

export interface ArtifactVersionInformation {
	version: string;
	parentOf: ArtifactDependencyVersionInformation;
	withChildren: ArtifactDependencyVersionInformation;
	properties: ArtifactDependencyVersionInformation2;
}

export interface ArtifactNameSubstitute {
	version?: ArtifactVersion;
	realName?: string;
	substitute?: string;
}

export interface ArtifactNameSubstitute2 {
	version?: string;
	realName?: string;
	substitute?: string;
}

export type ArtifactNameSubstituteType = ArtifactNameSubstitute | false;
export type ArtifactNameSubstituteType2 = ArtifactNameSubstitute2 | false;

export type ArtifactDependencyVersionInformation = Map<string, ArtifactNameSubstituteType>;
export type ArtifactDependencyVersionInformation2 = Map<string, ArtifactNameSubstituteType2>;

export interface FileUploadRequest {
	branch: string;
	author_mail: string;
	author_name: string;
	content: string;
	commit_message: string;
}

export interface FileUploadResponse {
	file_path: string;
	branch: string;
}

export interface AutoMergeRequest {
	merge_commit_message?: string;
	merge_when_pipeline_succeeds?: boolean;
	should_remove_source_branch?: boolean;
	squash_commit_message?: string;
	squash?: boolean;
}

export interface MergeRequest {
    source_branch: string;
    target_branch: string;
    title: string;
}

export interface NewBranchResponse {
	'commit': {
		'id': string;
		'short_id': string;
		'created_at': string;
		'parent_ids': string[];
		'title': string;
		'message': string;
		'author_name': string;
		'author_email': string;
		'authored_date': string;
		'committer_name': string;
		'committer_email': string;
		'committed_date': string;
		'web_url': string;
	},
	'name': string;
	'merged': boolean;
	'protected': boolean;
	'default': boolean;
	'developers_can_push': boolean;
	'developers_can_merge': boolean;
	'can_push': boolean;
	'web_url': string;
}

export interface MergeRequestResponse {
	id: number;
	iid: number;
	project_id: number;
	title: string;
	web_url: string;
	author: {
		id: number;
		name: string;
		username: string;
		state: string;
		avatar_url: string;
		web_url: string;
	},
}

export interface TreeFileInfo {
	id: string;
	name: string;
	type: string;
	path: string;
	mode: string;
}

export type TreeStructure = { [key: string]: number | TreeStructure };

export type ProjectChanges = ProjectChangeTag[];

export interface ProjectChangeTag {
	version: string;
	feat: ProjectChange[];
	bugfix: ProjectChange[];
	chore: ProjectChange[];
}

export interface ProjectChange {
	hash: string;
	author: string;
	committer: string;
	message: string;
}

export interface ModuleInfoDeps {
    uses: string[];
    providers: { [key: string]: string[] };
}
