import fs from 'fs';

import { getLogger, Logger } from './logger';
import { ModuleInfoDeps } from './types';

const logger: Logger = getLogger('module-info-processor');

function resolveFullImportName(content: string, className: string): string {
	let innerClass = undefined;
	if (className.includes('.')) {
		const classNameSplit = className.split('.');
		innerClass = classNameSplit.pop();
		className = <string>classNameSplit.pop();
	}

	logger.info(`Searching for className: ${className}`);
	const importRegex = new RegExp('import (?<fullClassName>[\\w.]*' + className + ')\\.?(?<innerClassName>[A-Z]\\w*)?;');
	const result: RegExpExecArray | null = importRegex.exec(content);

	if (!result) {
		logger.warn(`Could not identify full class name for import: '${className}'!`);
		return className;
	}

	const groups = result.groups;
	if (groups === undefined) {
		logger.warn('Groups were not found!');
		return className;
	}

	let fullClassName = <string>groups['fullClassName'];
	if (innerClass) {
		fullClassName += `.${innerClass}`;
	} else {
		if (groups['innerClassName']) {
			fullClassName += `.${groups['innerClassName']}`;
		}
	}

	return fullClassName;
}



export async function processModuleInfo(content: string, projectId: number, innerProjectPath: string[]): Promise<void> {
	const moduleInfoDeps: ModuleInfoDeps = {
		uses: [],
		providers: {}
	};

	const usesRegex = /uses (?<usesEntry>[\w.]*);/gm;
	let usesEntries;
	while ((usesEntries = usesRegex.exec(content)) !== null) {
		const groups = usesEntries.groups;
		if (!groups) continue;
		const usesEntry = groups['usesEntry'];
		if (usesEntry?.includes('.')) {
			moduleInfoDeps.uses.push(usesEntry);
		} else {
			const parsedUses = usesEntries.pop()?.split('.').pop();
			if (parsedUses !== undefined) {
				moduleInfoDeps.uses.push(resolveFullImportName(content, parsedUses));
			}
		}
	}

	let providesEntries;
	const providesRegex = /provides\s*([\w.]*)\s*with\s*([\w,\s.]*);/gm;
	while ((providesEntries = providesRegex.exec(content)) !== null) {
		const parsedProviders = providesEntries.pop()?.split(',')
			.map(entry => entry.trim())
			.map(provider => resolveFullImportName(content, provider)); // first entry represents tested string
		const provider = providesEntries.pop(); // second entry is the provider
		if (parsedProviders !== undefined && provider !== undefined) {
			let fullProviderName = '';
			if (provider.includes('.')) {
				fullProviderName = provider;
			} else {
				fullProviderName = resolveFullImportName(content, provider);
			}

			moduleInfoDeps.providers[fullProviderName] = parsedProviders;
		}
	}

	if (moduleInfoDeps.uses.length === 0 && Object.keys(moduleInfoDeps.providers).length === 0) {
		logger.verbose('File module-info.json will not be created because module does not have any dependencies or providers.');
		return;
	}

	const filePathParts = ['runtime', projectId, ...innerProjectPath];
	if (!fs.existsSync(filePathParts.join('/'))) {
		fs.mkdirSync(filePathParts.join('/'), { recursive: true });
	}

	filePathParts.push('module-info.json');
	await fs.promises.writeFile(filePathParts.join('/'), JSON.stringify(moduleInfoDeps), { encoding: 'utf-8', flag: 'w' });
	logger.info(`File module-info.json was created on path: ${filePathParts.join('/')}.`);
}
