export class ArtifactVersion {

	private static readonly VERSION_REGEX = /(?<major>\d+).(?<minor>\d+).(?<patch>\d+)(?<tail>-SNAPSHOT)?/;

	public readonly major: number;
	public readonly minor: number;
	public readonly patch: number;
	public readonly tail: string;

	constructor(public readonly rawVersion: string) {
		if (this.rawVersion === 'unknown') {
			this.major = this.minor = this.patch = 0;
			this.tail = '';
			return;
		}

		const exec = ArtifactVersion.VERSION_REGEX.exec(rawVersion);
		if (!exec)
			throw new Error(`Could not create version for input: '${rawVersion}'!`);
		const { major, minor, patch, tail } = (exec.groups as unknown as { major: number, minor: number, patch: number, tail: string});
		this.major = major;
		this.minor = minor;
		this.patch = patch;
		this.tail = tail ?? '';
	}

	compareVersions(other: ArtifactVersion): number {
		if (this.rawVersion === other.rawVersion) {
			return 0
		}

		return this.comparePartial(this.major, other.major)
			|| this.comparePartial(this.minor, other.minor)
			|| this.comparePartial(this.patch, other.patch)
			|| this.compareTail(this.tail, other.tail);
	}

	protected comparePartial(lhs: number, rhs: number): number {
		const delta = lhs - rhs;
		return delta === 0 ? 0 : delta > 0 ? 1 : -1;
	}

	protected compareTail(lhs: string, rhs: string): number {
		if (lhs === rhs) return 0;

		if (!rhs || rhs === '') {
			return -1;
		}

		return 1;
	}

	toString(): string {
		return this.rawVersion;
	}
}