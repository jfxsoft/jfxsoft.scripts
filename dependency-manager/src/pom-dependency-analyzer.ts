import { ArtifactVersion } from './artifact-version';
import { EnvironmentProvider } from './environment-provider';
import { GitlabFetcher } from './gitlab-fetcher';
import { getLogger, Logger } from './logger';
import {
	ArtifactDependencyVersionInformation2,
	ArtifactNameSubstitute,
	ArtifactNameSubstituteType,
	ArtifactNameSubstituteType2,
	ArtifactVersionInformation,
	DependencyGraph,
	POM,
	PomProperrties,
	Projects
} from './types';
import { readPOM } from './util';

export class PomDependencyAnalyzer {

	protected readonly regex: RegExp = /^project\.(?<name>.*)\.version$/;

	protected readonly logger: Logger = getLogger(PomDependencyAnalyzer.name);

	protected readonly dependencyGraph: DependencyGraph = new Map<string, ArtifactVersionInformation>();

	protected projects: Projects | undefined;

	public constructor(protected readonly gitlabFetcher: GitlabFetcher,
                       protected readonly envProvider: EnvironmentProvider) {
	}

	public async analyze(projects: Projects) {
		this.projects = projects;
		for (const project of projects) {
			this.logger.info(`Processing project name: '${project.name}'.`);
			const sourceBranch = this.envProvider.getPR_SOURCE_BRANCH() ?? project.default_branch;
			const pomInformation = await this.gitlabFetcher.fetchPom(project.id, sourceBranch);
			if (!pomInformation) {
				this.logger.warn(`The project with id: ${project.id} does not contain pom.xml file!`);
				continue;
			}

			const { pom, raw } = readPOM(pomInformation);

			this.analyzePOM(pom);
			project.pom = raw;
		}
	}

	protected analyzePOM(pom: POM) {
		const projectXML = pom.project;
		const parentXML = projectXML.parent;

		const parentArtifactId = parentXML?.artifactId;
		const currentArtifactId = projectXML.artifactId;

		if (parentArtifactId) {
			const artifactInformation = PomDependencyAnalyzer.extractDependencies(this.dependencyGraph, parentArtifactId);
			let parentOf = artifactInformation.parentOf.get(currentArtifactId);
			if (!parentOf) {
				parentOf = { realName: currentArtifactId };
				artifactInformation.parentOf.set(currentArtifactId, parentOf);
			}

			parentOf.version = new ArtifactVersion(parentXML?.version ?? 'unknown');
		}

		const dependencyInformation = PomDependencyAnalyzer.extractDependencies(this.dependencyGraph, currentArtifactId);
		dependencyInformation.version = projectXML.version ?? 'unknown';

		const repositories = projectXML?.repositories?.repository ?? [];

		if (Array.isArray(repositories)) {
			const array = repositories as { id: string; url: string; }[];
			for (const repository of array) {
				if (repository.id !== parentArtifactId) {
					this.setDependency(dependencyInformation, currentArtifactId, repository.id);
				}
			}
		} else {
			const repository = repositories as { id: string; url: string; };
			this.setDependency(dependencyInformation, currentArtifactId, repository.id);
		}

		if (dependencyInformation) {
			dependencyInformation.version = projectXML.version ?? 'unknown';
			this.analyzeDependencyVersions(dependencyInformation.properties, projectXML.properties ?? {});
			this.resolveUnknownDependencyVersion(dependencyInformation);
		}
	}

	protected setDependency(dependencyInformation: ArtifactVersionInformation, currentArtifact: string, childArtifact: string) {
		const parentArtifactInformation = PomDependencyAnalyzer.extractDependencies(this.dependencyGraph, childArtifact);
		if (parentArtifactInformation.parentOf.has(currentArtifact)) {
			dependencyInformation.withChildren.set(childArtifact, parentArtifactInformation.parentOf.get(currentArtifact) ?? false);
		} else {
			dependencyInformation.withChildren.set(childArtifact, false);
		}
	}

	protected analyzeDependencyVersions(dependencyVersions: ArtifactDependencyVersionInformation2, properties: PomProperrties) {
		for (const propertiesKey in properties) {
			dependencyVersions.set(propertiesKey, { version: properties[propertiesKey] });
		}
	}

	protected resolveUnknownDependencyVersion(dependencyInformation: ArtifactVersionInformation) {
		for (const [dependency] of dependencyInformation.withChildren.entries()) {

			let success = false;
			this.logger.info(`Found unknown version for dependency: ${dependency}.`);
			for(const [childDependency, childDependencyVersion] of dependencyInformation.properties.entries()) {
				if (!childDependencyVersion) { continue; }
				const exec: RegExpExecArray | null = this.regex.exec(childDependency);
				if (!exec) {
					continue;
				}
				const dependencyPartialName = (exec.groups as { name: string }).name ?? false;
				if (!dependencyPartialName) {
					this.logger.warn(`Dependency: ${dependency} not found!`);
					continue;
				}

				if (!dependency.endsWith(dependencyPartialName) && !dependency.endsWith(dependencyPartialName + '.parent')) { continue; }

				childDependencyVersion.substitute = childDependency;
				this.logger.info(`Resolved unknown dependency of: ${dependency} to version: ${JSON.stringify(childDependencyVersion)}.`);
				dependencyInformation.withChildren.set(dependency, {
					realName: childDependencyVersion.realName ?? dependency,
					substitute: childDependencyVersion.substitute,
					version: new ArtifactVersion(childDependencyVersion.version ?? 'unknown')
				});
				success = true;
				break;
			}

			if (!success) {
				this.logger.error(`Could not resolve unknown version dependency of: ${dependency}!`);
			}
		}
	}

	public getDependencyInformationFor(project: string): ArtifactVersionInformation | false {
		const originalProjectArtifactVersionInfoOrFalse = this.dependencyGraph.get(project) ?? false;
		if (!originalProjectArtifactVersionInfoOrFalse) {
			return false;
		}

		const originalProjectArtifactVersionInfo: ArtifactVersionInformation = <ArtifactVersionInformation>originalProjectArtifactVersionInfoOrFalse;
		const projectArtifactVersionInformation: ArtifactVersionInformation = {
			version: originalProjectArtifactVersionInfo.version,
			parentOf: originalProjectArtifactVersionInfo.parentOf,
			properties: originalProjectArtifactVersionInfo.properties,
			withChildren: new Map()
		};

		for (const [projectName, artifactVersionInfo] of this.dependencyGraph) {
			if (project !== projectName && artifactVersionInfo.withChildren.has(project)) {
				projectArtifactVersionInformation.withChildren.set(projectName, <ArtifactNameSubstitute>artifactVersionInfo.withChildren.get(project));
			}
		}

		return projectArtifactVersionInformation;
	}

	protected static extractDependencies(dependencyGraph: DependencyGraph, dependencyKey: string): ArtifactVersionInformation {
		let dependencies: ArtifactVersionInformation;
		if (!dependencyGraph.has(dependencyKey)) {
			dependencies = {
				version: 'unknown',
				properties: new Map<string, ArtifactNameSubstituteType2>(),
				parentOf: new Map<string, ArtifactNameSubstituteType>(),
				withChildren: new Map<string, ArtifactNameSubstituteType>()
			};
			dependencyGraph.set(dependencyKey, dependencies);
		} else {
			dependencies = dependencyGraph.get(dependencyKey) as ArtifactVersionInformation;
		}

		return dependencies;
	}
}