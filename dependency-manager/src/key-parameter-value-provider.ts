import { getDeepProperty } from './properties-helper';
import { AdditionalParameters, ParameterValueProvider } from './text-renderer';

export class KeyParameterValueProvider implements ParameterValueProvider {

	private constructor(private readonly key: string) {
	}

	public static forKey(key: string): ParameterValueProvider {
		return new KeyParameterValueProvider(key);
	}

	canProvide(name: string, additionalParameters: AdditionalParameters): boolean {
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		return additionalParameters[this.key] !== undefined && getDeepProperty(additionalParameters, name.split('.')) !== undefined;
	}

	provide(name: string, additionalParameters: AdditionalParameters): string {
		return getDeepProperty(additionalParameters, name.split('.'));
	}

}