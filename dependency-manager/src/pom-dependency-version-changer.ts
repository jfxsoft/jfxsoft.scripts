import { getLogger, Logger } from './logger';

const logger: Logger = getLogger('PomDependencyVersionChanger');
const parentVersionRegex = /<parent>\n?\s*<groupId>.*<\/groupId>\n?\s*<artifactId>.*<\/artifactId>\n?\s*<version>(?<version>.*)<\/version>\n?\s*<\/parent>/;
const createChildVersionRegex = (dependencyName: string) => `<properties>(\n|.)*(?<dependency><${dependencyName}>(?<version>.*)</${dependencyName}>)(\n|.)*</properties>`;

export function changeParentDependencyVersion(pom: string, dependencyName: string, newVersion: string) {
	const regExpExecArray: RegExpExecArray | null = parentVersionRegex.exec(pom);
	if (!regExpExecArray) {
		logger.error(`Dependence: '${dependencyName}' is not identified as a parent dependency!`);
		return false;
	}

	const groups = regExpExecArray.groups;
	if (!groups) {
		return false;
	}

	const parentDefinitionStartIndex = regExpExecArray.index;
	const oldVersion = regExpExecArray.pop();
	const parentDependencyDefinition = regExpExecArray.pop();

	if (!(parentDependencyDefinition && oldVersion)) {
		return false;
	}

	let changedPom = pom.substring(0, parentDefinitionStartIndex);
	changedPom += parentDependencyDefinition.replace(oldVersion, newVersion);
	changedPom += pom.substring(parentDefinitionStartIndex + parentDependencyDefinition.length);

	return changedPom;
}

export function changeChildDependencyVersion(pom: string, dependencyName: string, newVersion: string) {
	const regExpExecArray: RegExpExecArray | null = new RegExp(createChildVersionRegex(dependencyName), 'gm').exec(pom);
	if (!regExpExecArray) {
		logger.info(`Dependence: '${dependencyName}' is not identified as a child dependency!`);
		return false;
	}

	const propertiesDefinitionStartIndex = regExpExecArray.index;
	regExpExecArray.pop(); // last empty match
	const oldVersion = regExpExecArray.pop();
	const foundDependency = regExpExecArray.pop();
	regExpExecArray.pop(); // another empty match
	const propertiesContent = regExpExecArray.pop();

	if (!(oldVersion && foundDependency && propertiesContent)) {
		return false;
	}

	const updatedFoundDependency = foundDependency.replace(oldVersion, newVersion);
	const updatedPropertiesContent = propertiesContent.replace(foundDependency, updatedFoundDependency);

	let newPom = pom.substring(0, propertiesDefinitionStartIndex);
	newPom += updatedPropertiesContent;
	newPom += pom.substring(propertiesDefinitionStartIndex + propertiesContent.length + 1);

	return newPom;
}
