import fs from 'fs';
import path from 'path';

import { getLogger, Logger } from './logger';
import { TreeStructure } from './types';

const logger: Logger = getLogger('main');

const ROOT_DIR = process.argv[2];
const DEST_DIR = process.argv[3];

async function copyFiles(docSource: string, docTarget: string, filter: (entry: fs.Dirent) => boolean): Promise<void> {
	if (!fs.existsSync(docTarget)) {
		fs.mkdirSync(docTarget);
	}

	const entries = fs.readdirSync(docSource, { withFileTypes: true });
	for (const entry of entries) {
		if (filter(entry)) {
			logger.info(`Copy file: ${entry.name} from: ${path.join(entry.path, entry.name)} -> ${path.join(docTarget, entry.name)}`);
			fs.copyFileSync(path.join(entry.path, entry.name), path.join(docTarget, entry.name));
			continue;
		}


		if (entry.isDirectory()) {
			await copyFiles(path.join(docSource, entry.name), path.join(docTarget, entry.name), filter);
		}
	}
}

async function handleTargetFiles(docSource: string, docTarget: string): Promise<void> {
	const entries = fs.readdirSync(docSource, { withFileTypes: true });
	for (const entry of entries) {
		if (entry.isDirectory()) {
			if (entry.name === 'screenshots') {
				logger.info('Handling screenshots...');
				await copyFiles(path.join(entry.path, entry.name), docTarget,
					e => e.isFile() && e.name.endsWith('.png'));
			}
		}
	}
}

async function exploreDocumentation(projectDocsPath: string, targetPath: string[]): Promise<void> {
	const entries = fs.readdirSync(projectDocsPath, { withFileTypes: true });

	for (const entry of entries) {
		if (entry.isDirectory()) {
			if (entry.name === 'docs') {
				const documentationTarget = path.join(...targetPath, entry.name);
				logger.info(`Handling directory: ${entry.path} -> ${documentationTarget}`);
				await copyFiles(path.join(entry.path, entry.name), path.join(...targetPath),
					e => e.isFile() && e.name.endsWith('.md'));
				continue;
			}
			if (entry.name === 'target') {
				logger.info(`Handling generated files in target: ${entry.path}`);
				await handleTargetFiles(path.join(entry.path, entry.name), path.join(...targetPath));
				continue;
			}
			if (entry.name.startsWith('jfxsoft')) {
				await exploreDocumentation(path.join(projectDocsPath, entry.name), [...targetPath, entry.name]);
			}
		}
	}
}

async function handleProjectRecursive(projects: TreeStructure): Promise<void> {
	for (const key of Object.keys(projects)) {
		const projectOrId = projects[key];
		if (typeof projectOrId === 'number') {
			const projectPath = [ROOT_DIR, key].join('/');
			logger.info(`Copy files for project: ${projectPath}`);

			if (!fs.existsSync(projectPath)) {
				logger.warn(`Skipping project: ${key}`);
				continue;
			}

			const dest = path.join(DEST_DIR, String(projectOrId))
			if (!fs.existsSync(dest)) {
				fs.mkdirSync(dest);
			}

			await exploreDocumentation(projectPath, [dest]);
		} else {
			await handleProjectRecursive(projectOrId);
		}
	}
}

async function main() {
	const projectTree: TreeStructure = JSON.parse(fs.readFileSync('runtime/project-tree.json', { encoding: 'utf-8' }));
	const jfxsoftProjects: TreeStructure = <TreeStructure>projectTree['jfxsoft'];

	await handleProjectRecursive(jfxsoftProjects);
}

main().catch((reason: Error) => {
	logger.error(reason.message);
	logger.error(reason.stack);
})
