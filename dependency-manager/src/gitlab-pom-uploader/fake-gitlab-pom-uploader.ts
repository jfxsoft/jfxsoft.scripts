import { getLogger, Logger } from '../logger';
import { MergeRequestResponse, Project } from '../types';

import { GitlabPomUploader } from './gitlab-pom-uploader';

export class FakeGitlabPomUploader implements GitlabPomUploader {

	protected readonly logger: Logger = getLogger(FakeGitlabPomUploader.name);

	public async uploadPom(project: Project, sourceBranch: string, branchName: string): Promise<MergeRequestResponse | false> {
		this.logger.info(`Uploading pom for project: '${project.name}'.`);
		this.logger.info(JSON.stringify({
			branch: `dep-update/${branchName}`,
			file_path: 'pom.xml'
		}));

		return Promise.resolve({
			id: -1,
			iid: -1,
			project_id: project.id,
			web_url: 'URL of the merge request',
			title: `Dependency update - ${branchName}`,
			author: {
				id: -1,
				name: 'fake_name',
				username: 'fake_username',
				state: 'active',
				web_url: 'url',
				avatar_url: 'avatar_url'
			}
		});
	}

}