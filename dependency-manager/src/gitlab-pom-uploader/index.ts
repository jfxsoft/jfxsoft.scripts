import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';

import { FakeGitlabPomUploader } from './fake-gitlab-pom-uploader';
import { RealGitlabPomUploader } from './real-gitlab-pom-uploader';

const logger: Logger = getLogger('GitlabPomUploader');

export function getGitlabPomUploader(envProvider: EnvironmentProvider, httpClient: HttpsClient) {
	if (envProvider.isCI()) {
		logger.info('Using RealGitlabPomUploader...');
		return new RealGitlabPomUploader(envProvider, httpClient);
	}

	logger.info('Using FakeGitlabPomUploader...');
	return new FakeGitlabPomUploader();
}

export * from './gitlab-pom-uploader';