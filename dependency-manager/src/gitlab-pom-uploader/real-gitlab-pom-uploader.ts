import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';
import {
	FileUploadRequest,
	FileUploadResponse,
	MergeRequestResponse,
	NewBranchResponse,
	Project
} from '../types';

import { GitlabPomUploader } from './gitlab-pom-uploader';

export class RealGitlabPomUploader implements GitlabPomUploader {

	protected readonly logger: Logger = getLogger(RealGitlabPomUploader.name);

	constructor(protected readonly envProvider: EnvironmentProvider,
		protected readonly httpClient: HttpsClient) {
	}

	public async uploadPom(project: Project, sourceBranch: string, branchName: string): Promise<MergeRequestResponse | false> {
		this.logger.info(`Fetching information about branch ${sourceBranch}...`);
		const branchExists = await this.httpClient.get(this.createSingleBranchInfoRequestUrl(project.id, sourceBranch));
		if (!branchExists) {
			sourceBranch = project.default_branch;
		}

		this.logger.info(`Creating new branch from ${sourceBranch} -> ${branchName}.`);
		const newBranchResponse: NewBranchResponse | false = await this.httpClient.post(this.createNewBranchRequestUrl(project.id, sourceBranch, `dep-update/${branchName}`));
		if (!newBranchResponse) {
			this.logger.error('New branch was not created -> pom will not be updated!');
			return false;
		}

		const payload: FileUploadRequest = {
			branch: newBranchResponse.name,
			author_name: this.envProvider.getGIT_USER_NAME(),
			author_mail: this.envProvider.getGIT_USER_EMAIL(),
			commit_message: 'Dependency update',
			content: project.pom
		}

		this.logger.info('Uploading new pom.xml file...');
		const uploadedFileInfo: FileUploadResponse | false = await this.httpClient.put(this.createPomRequestUrl(project.id), payload);
		if (!uploadedFileInfo) {
			this.logger.error('Could not update POM!');
			return false;
		}

		const mrTitle = `Dependency update - ${newBranchResponse.name}.`;
		this.logger.info(`Creating new merge request from ${sourceBranch} -> ${branchName}`);
		return await this.httpClient.post(this.createNewMergeRequestUrl(project.id, newBranchResponse.name, sourceBranch, mrTitle));
	}

	protected createPomRequestUrl(projectId: number) {
		return `projects/${projectId}/repository/files/pom.xml`;
	}

	protected createSingleBranchInfoRequestUrl(projectId: number, branch: string) {
		return `projects/${projectId}/repository/branches/${branch}`;
	}

	protected createNewBranchRequestUrl(projectId: number, sourceBranch: string, targetBranch: string) {
		return `projects/${projectId}/repository/branches?branch=${targetBranch}&ref=${sourceBranch}`;
	}

	protected createNewMergeRequestUrl(projectId: number, sourceBranch: string, targetBranch: string, title: string) {
		return `projects/${projectId}/merge_requests?source_branch=${sourceBranch}&target_branch=${targetBranch}&title=${title}`;
	}

}