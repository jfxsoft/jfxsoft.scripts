import { MergeRequestResponse, Project } from '../types';

export interface GitlabPomUploader {

	uploadPom(project: Project, sourceBranch: string, branchName: string): Promise<MergeRequestResponse | false>;

}