import { Logger as InternalLogger } from 'sitka';

const level = parseInt((process.env['FETCHER__LOG_LEVEL'] || '5'));

export function getLogger(name: string) {

	return InternalLogger.getLogger({ name, level });
}

export type Logger = InternalLogger;