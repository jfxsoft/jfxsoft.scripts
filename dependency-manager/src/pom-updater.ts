import { ArtifactVersion } from './artifact-version';
import { getLogger, Logger } from './logger';
import { changeChildDependencyVersion, changeParentDependencyVersion } from './pom-dependency-version-changer';
import { ArtifactNameSubstitute, Project, Projects } from './types';

const logger: Logger = getLogger('PomUpdater');

export function updatePomParentDependency(projects: Projects, dependencies: Map<string, ArtifactNameSubstitute>, currentProject: Project, releaseVersion: ArtifactVersion) {
	return updatePom(projects, dependencies, (dependentProject: Project) => {
		return changeParentDependencyVersion(dependentProject.pom, currentProject.name, releaseVersion.rawVersion);
	});
}

export function updatePomChildDependency(projects: Projects, dependencies: Map<string, ArtifactNameSubstitute>, currentProject: Project, releaseVersion: ArtifactVersion) {
	return updatePom(projects, dependencies, (dependentProject: Project, dependencyToBeUpdated: ArtifactNameSubstitute) => {
		return changeChildDependencyVersion(dependentProject.pom, dependencyToBeUpdated.substitute || '', releaseVersion.rawVersion);
	});
}

function updatePom(projects: Projects, dependencies: Map<string, ArtifactNameSubstitute>, performUpdate: UpdateFunction): Set<Project> {
	const updatedProjects = new Set<Project>;

	for (const [dependencyOrProjectName, dependencyOrProjectNameSubstitute] of dependencies) {
		const updatingProject: Project | false = projects.find(project => project.name === dependencyOrProjectName) ?? false;
		if (!updatingProject) {
			logger.error(`Could not find dependent project/dependency: ${dependencyOrProjectNameSubstitute.realName}`);
			continue;
		}

		const updatedPomOrFail = performUpdate(updatingProject, dependencyOrProjectNameSubstitute);
		if (!updatedPomOrFail) {
			logger.error(`Could not update project/dependency: ${JSON.stringify(dependencyOrProjectName)} of pom in project/dependency: '${updatingProject.name}'!`);
			continue;
		}

		updatingProject.pom = <string>updatedPomOrFail;
		logger.info('Project/Dependency has been updated successfully.');
		updatedProjects.add(updatingProject);
	}

	return updatedProjects;
}

type UpdateFunction = (dependentProject: Project, dependencyToBeUpdated: ArtifactNameSubstitute) => string | boolean;
