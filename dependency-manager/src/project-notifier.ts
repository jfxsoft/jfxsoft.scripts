import { readFileSync } from 'fs';

import { getProperties, KeyValuePairObject } from 'properties-file';

import { ArtifactVersion } from './artifact-version';
import { selectDependenciesForUpdate } from './dependency-update-selector';
import { EnvironmentProvider } from './environment-provider';
import { getGitlabFetcherInstance, GitlabFetcher } from './gitlab-fetcher';
import { getGitlabMergeRequesterInstance, GitlabMergeRequester } from './gitlab-merge-requester';
import { getGitlabPomUploader, GitlabPomUploader } from './gitlab-pom-uploader';
import { HttpsClient } from './https-client';
import { KeyParameterValueProvider } from './key-parameter-value-provider';
import { getLogger, Logger } from './logger';
import { PomDependencyAnalyzer } from './pom-dependency-analyzer';
import { updatePomChildDependency, updatePomParentDependency } from './pom-updater';
import { EnvironmentValueProvider } from './text-renderer/environment-value-provider';
import { ReleasePropertiesProvider } from './text-renderer/release-properties-provider';
import { ArtifactNameSubstitute, MergeRequestResponse, Project } from './types';

const logger: Logger = getLogger('main');

async function main() {
	const envProvider: EnvironmentProvider = new EnvironmentProvider();
	const httpClient: HttpsClient = new HttpsClient(envProvider);
	const gitlabFetcher: GitlabFetcher = getGitlabFetcherInstance(envProvider, httpClient);
	const pomDependencyAnalyzer: PomDependencyAnalyzer = new PomDependencyAnalyzer(gitlabFetcher, envProvider);
	const properties: KeyValuePairObject = getProperties(readFileSync(envProvider.getCI_PROJECT_DIR() + '/release.properties.bak'));

	const waitTime = envProvider.getWAIT_TIME();

	if (envProvider.isCI()) {
		logger.info(`Waiting ${waitTime} ms.`);
		await new Promise<void>(resolve => {
			setTimeout(() => {
				resolve();
			}, waitTime);
		});
	}

	const mainGroup = await gitlabFetcher.fetchMainGroup();
	if (!mainGroup) {
		logger.error('Could not obtain main group of the project...');
		return;
	}
	logger.info(`Main group id: ${mainGroup.id}, name: '${mainGroup.name}'.`);

	const projects = await gitlabFetcher.fetchProjects(mainGroup);
	if (!projects) {
		logger.error('Could not obtain projects in main group...')
		return;
	}
	logger.info(`Loaded #${projects.length} projects.`);
	const currentProject: Project | undefined = projects.find(project => envProvider.getCI_PROJECT_NAME() === project.name);
	if (!currentProject) {
		logger.error(`Could not find current project with name: '${envProvider.getCI_PROJECT_NAME()}'!`);
		return;
	}

	await pomDependencyAnalyzer.analyze(projects);

	const dependencyInformation = pomDependencyAnalyzer.getDependencyInformationFor(currentProject.name);
	if (!dependencyInformation) {
		logger.error(`Dependencies for current project: ${currentProject.name} not found!`);
		return;
	}

	const releaseVersion: ArtifactVersion = new ArtifactVersion(getReleaseVersion(envProvider, properties));
	logger.info(`Project information for: ${currentProject.name}.`)
	logger.info(`Snapshot version: ${dependencyInformation.version}, Release version: ${releaseVersion}`);
	logger.trace(Object.fromEntries([...dependencyInformation.parentOf.entries()]));
	logger.trace(Object.fromEntries([...dependencyInformation.withChildren.entries()]));

	logger.info('Selecting parent dependency candidates for update.');
	const parentDependenciesToBeUpdated: Map<string, ArtifactNameSubstitute> = selectDependenciesForUpdate(dependencyInformation.parentOf, releaseVersion);
	logger.info('Selecting child dependency candidates for update.');
	const childDependenciesToBeUpdated: Map<string, ArtifactNameSubstitute> = selectDependenciesForUpdate(dependencyInformation.withChildren, releaseVersion);

	if (parentDependenciesToBeUpdated.size === 0 && childDependenciesToBeUpdated.size === 0) {
		logger.info('No dependency needs to be updated.');
		return;
	}

	const updatedProjects: Set<Project> = new Set<Project>;
	updatePomParentDependency(projects, parentDependenciesToBeUpdated, currentProject, releaseVersion)
		.forEach((project: Project) => updatedProjects.add(project));
	updatePomChildDependency(projects, childDependenciesToBeUpdated, currentProject, releaseVersion)
		.forEach((project: Project) => updatedProjects.add(project));

	const mergeRequests: Set<MergeRequestResponse> = new Set<MergeRequestResponse>();

	const pomUploader: GitlabPomUploader = getGitlabPomUploader(envProvider, httpClient);
	for (const updatedProject of updatedProjects) {
		logger.info(`Processing update of project: ${updatedProject.name}.`);
		const sourceBranch = envProvider.getPR_SOURCE_BRANCH() ?? updatedProject.default_branch;
		const mergeRequestResponse = await pomUploader.uploadPom(updatedProject, sourceBranch, properties['scm.tag']);
		if (!mergeRequestResponse) {
			logger.error('Could not create merge request!');
		} else {
			mergeRequests.add(<MergeRequestResponse><unknown>mergeRequestResponse);
		}
	}

	if (!envProvider.getAUTO_MERGE_REQUEST()) {
		logger.info('Merge requests will not be automatically merged.');
		return;
	}

	logger.info('Waiting 3s.');
	await new Promise<void>(resolve => {
		setTimeout(() => {
			resolve();
		}, 3000);
	});

	const mergeRequester: GitlabMergeRequester = getGitlabMergeRequesterInstance(envProvider, httpClient, projects, [
		KeyParameterValueProvider.forKey('projects'),
		ReleasePropertiesProvider.forProperties(properties),
		EnvironmentValueProvider.INSTANCE
	]);

	for (const mergeRequest of mergeRequests) {
		await mergeRequester.autoMergeRequest(mergeRequest);
	}
}

function getReleaseVersion(envProvider: EnvironmentProvider, properties: KeyValuePairObject) {
	const branchTag = properties['scm.tag'];
	const projectName = envProvider.getCI_PROJECT_NAME() ?? '';
	return branchTag.replace(projectName + '-', '');
}

main().catch((reason: Error) => {
	logger.error(reason.message);
	logger.error(reason.stack);
})
