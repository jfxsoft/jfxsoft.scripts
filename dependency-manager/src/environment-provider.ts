export class EnvironmentProvider {

	public isCI(): boolean {
		return (process.env['CI'] && true) || false;
	}

	public getCI_PROJECT_NAME() {
		return process.env['CI_PROJECT_NAME'];
	}

	public getCI_PROJECT_DIR() {
		return process.env['CI_PROJECT_DIR'];
	}

	public getGROUP_NAME() {
		return process.env['CI_PROJECT_ROOT_NAMESPACE'];
	}

	public getCUSTOM_TOKEN(): string {
		return process.env['CUSTOM_TOKEN'] || '';
	}

	public getSTORE_RESPONSES(): boolean {
		return (process.env['STORE_RESPONSES'] && true) || false;
	}

	public getGIT_USER_EMAIL(): string {
		return process.env['GIT_USER_EMAIL'] ?? 'GIT_USER_EMAIL';
	}

	public getGIT_USER_NAME(): string {
		return process.env['GIT_USER_NAME'] ?? 'GIT_USER_NAME';
	}

	public getWAIT_TIME(): number {
		return parseInt(process.env['WAIT_TIME'] ?? '90000');
	}

	public getPR_SOURCE_BRANCH() {
		return process.env['PR_SOURCE_BRANCH'];
	}

	public getAUTO_MERGE_REQUEST(): boolean {
		return (process.env['AUTO_MERGE_REQUEST'] && true) || false
	}

	public getCOMMIT_MESSAGE() {
		return process.env['COMMIT_MESSAGE'];
	}

	public getSQUASH_MESSAGE() {
		return process.env['SQUASH_MESSAGE'];
	}

	public getCHANGES_FILE_NAME() {
		return process.env['CHANGES_FILE_NAME'];
	}

	public getDOCUMENTATION_BRANCH_NAME_SOURCE() {
		return process.env['DOCUMENTATION_BRANCH_NAME_SOURCE'] ?? 'main';
	}
}
