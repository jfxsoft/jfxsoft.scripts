// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getDeepProperty<T>(obj: any, props: string[]): T {
	return obj && props.reduce(
		(result, prop) => result == null ? undefined : result[prop],
		obj
	);
}