import { XMLParser } from 'fast-xml-parser';

import { FileInformation, POM } from './types';

const xmlParser: XMLParser = new XMLParser();

export function readPOM(pomInformation: FileInformation): { pom: POM, raw: string } {
	const pom = pomInformation.content;
	const buff = Buffer.from(pom, 'base64');
	const text = buff.toString('ascii');
	return { pom: xmlParser.parse(text), raw: text };
}