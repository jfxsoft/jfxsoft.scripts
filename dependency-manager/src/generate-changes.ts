import * as child_process from 'child_process';
import fs from 'fs';
import path from 'path';

import { XMLParser } from 'fast-xml-parser';

import { getLogger, Logger } from './logger';
import { POM, ProjectChange, ProjectChanges, ProjectChangeTag } from './types';

const logger: Logger = getLogger('main');
const descriptorRegex: RegExp = /(.*)\/(.*) (.*) \((feat|bugfix|chore)\) (.+)/;

async function readReleaseVersion(pomFilePath: string): Promise<string> {
	const parser: XMLParser = new XMLParser();
	const pomXml: POM = parser.parse(fs.readFileSync(pomFilePath, { encoding: 'utf-8' }));
	const version: string = pomXml.project.version || 'unknown';

	return version.replace('-SNAPSHOT', '');
}

async function main() {
	if (process.argv.length !== 5) {
		logger.info(process.argv);
		logger.error('Tag, working directory and name of changes file must be provided as an argument!');
		return -1;
	}

	const lastTag = process.argv[2];
	const projectDir = path.resolve(process.argv[3]);
	const changesFilePath = process.argv[4];
	logger.info(`Resolved project directory: ${projectDir}`);
	logger.info(`Changes file: ${changesFilePath}`);

	if (!fs.existsSync(path.resolve(projectDir, changesFilePath))) {
		fs.writeFileSync(path.resolve(projectDir, changesFilePath), '[]', { encoding: 'utf-8', flag: 'wx' });
	}

	const changes: ProjectChanges = JSON.parse(fs.readFileSync(path.resolve(projectDir, changesFilePath), { encoding: 'utf-8' }));

	if (changes.find(tag => tag.version === lastTag)) {
		logger.info(`Changes for version ${lastTag} are already generated.`);
		return 1;
	}

	const releaseVersion: string = await readReleaseVersion(path.resolve(projectDir, 'pom.xml'));

	const tag: ProjectChangeTag = {
		version: releaseVersion,
		feat: [],
		bugfix: [],
		chore: []
	};

	let gitLogTag: string = 'HEAD';
	if (lastTag !== 'no_tag') {
		gitLogTag = `${lastTag}..${gitLogTag}`;
	}
	const gitLog = child_process.execSync(`git log ${gitLogTag} --pretty="format:%aN/%cN %h %s"`, { cwd: projectDir });

	for (const line of gitLog.toString().split('\n')) {
		const match: RegExpMatchArray | null = line.match(descriptorRegex);
		if (match === null) {
			continue;
		}

		const author: string = match[1];
		const committer: string = match[2];
		const hash: string = match[3];
		const type: string = match[4];
		const message: string = match[5];

		const typeChangeKey: keyof ProjectChangeTag= type as keyof typeof tag;
		const typeChanges: ProjectChange[] = <ProjectChange[]><unknown>tag[typeChangeKey];
		const change: ProjectChange = {
			author, committer, hash, message
		};

		typeChanges.push(change);
	}

	Object.keys(tag).forEach(key => {
		const typeKey: keyof ProjectChangeTag= key as keyof typeof tag;
		if (Array.isArray(tag[typeKey])) {
			if (tag[typeKey].length === 0) {
				logger.info(`${key}: No changes`);
				delete tag[typeKey];
			} else {
				for (const change of tag[typeKey]) {
					logger.info(`${key}: ${JSON.stringify(change)}`);
				}
			}
		}
	})

	changes.push(tag);

	fs.writeFileSync(path.resolve(projectDir, changesFilePath), JSON.stringify(changes), { encoding: 'utf-8' });

	return 0;
}

main().catch((reason: Error) => {
	logger.error(reason.message);
	logger.error(reason.stack);
}).then(result => {
	if (result == null) {
		result = -1;
	}

	child_process.execSync(`export CHANGES_EXISTS=${result === 0 ? '0' : '1'}`);
})
