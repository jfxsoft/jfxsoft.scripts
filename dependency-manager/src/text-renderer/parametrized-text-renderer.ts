import { AdditionalParameters, ParameterValueProvider } from './parameter-value-provider';

export class ParametrizedTextRenderer {

	private readonly text: string[];

	public constructor(text: string) {
		this.text = text.split(' ');
	}

	public render(valueProviders: ParameterValueProvider[], additionalParams: AdditionalParameters): string {
		return this.text.reduce((prev: string, curr: string) => {
			let value: string | undefined = undefined;

			// handle values only with $ in the beginning
			if (curr.startsWith('$')) {
				// remove $ character
				curr = curr.substring(1);
				value = valueProviders.find((provider: ParameterValueProvider) => provider.canProvide(curr, additionalParams))?.provide(curr, additionalParams);
			}

			return prev + ' ' + (value !== undefined ? <string>value : curr);
		});
	}
}
