import { ParameterValueProvider } from './parameter-value-provider';

export class EnvironmentValueProvider implements ParameterValueProvider {

	public static readonly INSTANCE = new EnvironmentValueProvider();

	canProvide(name: string): boolean {
		return process.env[name] !== undefined;
	}

	provide(name: string): string {
		return process.env[name] || '';
	}

}