export type AdditionalParameters = {
    [key: string]: unknown
};

export interface ParameterValueProvider {

    canProvide(name: string, additionalParameters: AdditionalParameters): boolean;

    provide(name: string, additionalParameters: AdditionalParameters): string;

}