import { KeyValuePairObject } from 'properties-file';

import { ParameterValueProvider } from './parameter-value-provider';

export class ReleasePropertiesProvider implements ParameterValueProvider {

	private readonly properties: KeyValuePairObject;

	private constructor(properties: KeyValuePairObject) {
		this.properties = properties;
	}

	public static forProperties(properties: KeyValuePairObject) {
		return new ReleasePropertiesProvider(properties);
	}

	canProvide(name: string): boolean {
		return this.properties[name] !== undefined;
	}

	provide(name: string): string {
		return this.properties[name];
	}

}