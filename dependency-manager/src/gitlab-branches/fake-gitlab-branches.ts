import { GitlabBranches } from './gitlab-branches';

export class FakeGitlabBranches implements GitlabBranches {

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public async branchExists(projectId: number, name: string): Promise<boolean> {
		return Promise.resolve(true);
	}

}