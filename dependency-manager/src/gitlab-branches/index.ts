import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';

import { FakeGitlabBranches } from './fake-gitlab-branches';
import { RealGitlabBranches } from './real-gitlab-branches';

const logger: Logger = getLogger('GitlabFetcherResolver');

export function getGitlabBranchesInstance(envProvider: EnvironmentProvider, httpClient: HttpsClient) {
	if (envProvider.isCI()) {
		logger.info('Using RealGitlabFetcher...');
		return new RealGitlabBranches(envProvider, httpClient);
	}

	logger.info('Using FakeGitlabFetcher...');
	return new FakeGitlabBranches();
}

export * from './gitlab-branches';