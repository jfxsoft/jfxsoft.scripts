import { EnvironmentProvider } from '../environment-provider';
import { HttpsClient } from '../https-client';
import { getLogger, Logger } from '../logger';

import { GitlabBranches } from './gitlab-branches';

export class RealGitlabBranches implements GitlabBranches {

	private static logger: Logger = getLogger('RealGitlabBranches');

	public constructor(protected readonly envProvider: EnvironmentProvider,
                       protected readonly httpClient: HttpsClient) {}

	public async branchExists(projectId: number, name: string): Promise<boolean> {
		return this.httpClient.get<boolean>(this.createBranchRequestUrl(projectId, name));
	}

	protected createBranchRequestUrl(projectId: number, branchName: string) {
		return `projects/${projectId}/repository/branches/${branchName}`;
	}
    
}