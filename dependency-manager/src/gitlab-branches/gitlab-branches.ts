export interface GitlabBranches {

    branchExists(projectId: number, name: string): Promise<boolean>;

}