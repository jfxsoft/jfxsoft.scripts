import { ArtifactVersion } from './artifact-version';
import { getLogger, Logger } from './logger';
import { ArtifactDependencyVersionInformation, ArtifactNameSubstitute } from './types';

const logger: Logger = getLogger('DependencyUpdateSelector');

export function selectDependenciesForUpdate(dependencies: ArtifactDependencyVersionInformation, releaseVersion: ArtifactVersion): Map<string, ArtifactNameSubstitute> {
	const dependenciesToBeUpdated: Map<string, ArtifactNameSubstitute> = new Map<string, ArtifactNameSubstitute>();

	for (const [depName, depInfo] of dependencies) {
		if (!depInfo || !depInfo.version) { continue;}

		if (releaseVersion.compareVersions(depInfo.version)) {
			dependenciesToBeUpdated.set(depName, <ArtifactNameSubstitute>depInfo);
			//dependenciesToBeUpdated.push(<ArtifactNameSubstitute>dependencies.get(depName));
			logger.info(`Dependency: ${depName} needs to be updated from version: '${depInfo.version.toString()}' to version: '${releaseVersion.toString()}'.`);
		}
	}

	return dependenciesToBeUpdated;
}