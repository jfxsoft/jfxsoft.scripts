import fs from 'fs';
import path from 'path';

import { EnvironmentProvider } from './environment-provider';
import { getGitlabBranchesInstance, GitlabBranches } from './gitlab-branches';
import { getGitlabFetcherInstance, GitlabFetcher } from './gitlab-fetcher';
import { HttpsClient } from './https-client';
import { getLogger, Logger } from './logger';
import { processModuleInfo } from './module-info-processor';
import { Project, Projects, TreeStructure } from './types';
import { readPOM } from './util';

const logger: Logger = getLogger('main');

async function fetchDocs(project: Project, branchName: string, gitlabFetcher: GitlabFetcher, innerProjectPath: string[] = []): Promise<void> {
	logger.info(`Fetching docs for project: ${project.name}`);
	const docs = await gitlabFetcher.fetchDocsFilesInfo(project.id, branchName, innerProjectPath);
	if (!docs) {
		logger.verbose('Project does not contain any docs, skipping...');
		return;
	}

	for (const docInfo of docs) {
		const docFile = await gitlabFetcher.fetchDocFile(project.id, branchName, docInfo.path);
		if (!docFile) {
			logger.error(`Could not fetch doc file: ${docInfo.path}!`);
		}
	}
}

async function fetchModuleInfo(project: Project, branchName: string, gitlabFetcher: GitlabFetcher, innerProjectPath: string[] = []): Promise<void> {
	const moduleName = innerProjectPath.length !== 0 ? innerProjectPath[innerProjectPath.length - 1] : project.name;
	logger.info(`Fetching module-info.java for project: ${moduleName}`);
	const moduleInfo = await gitlabFetcher.fetchModuleInfoFileInfo(project.id, branchName, innerProjectPath);
	if (!moduleInfo) {
		logger.verbose('Project does not contain module-info.java file, skipping...');
		return;
	}

	const content = Buffer.from(moduleInfo.content, 'base64').toString('utf-8');

	await processModuleInfo(content, project.id, innerProjectPath);
}

async function processSubmodulesRecursive(project: Project, branchName: string, submodules: string[], gitlabFetcher: GitlabFetcher, parentPath: string[] = []): Promise<TreeStructure> {
	const parentTree: TreeStructure = {};
	for (const submodule of submodules) {

		const submodulePathWithParent = [...parentPath, submodule];
		logger.info(`\tProcessing inner submodule with name: ${submodule}`);
		const innerPomInformation = await gitlabFetcher.fetchPom(project.id, branchName, submodulePathWithParent.join('/'));
		if (!innerPomInformation) {
			logger.warn(`\tPom for submodule: ${submodule} was not found!`);
			continue;
		}

		await fetchDocs(project, branchName, gitlabFetcher, submodulePathWithParent);
		await fetchModuleInfo(project, branchName, gitlabFetcher, submodulePathWithParent);

		const innerPom = readPOM(innerPomInformation);
		let moreSubmodules = innerPom.pom.project.modules?.module || [];
		if (!Array.isArray(moreSubmodules)) {
			moreSubmodules = [moreSubmodules];
		}

		parentTree[submodule] = await processSubmodulesRecursive(project, branchName, moreSubmodules, gitlabFetcher, submodulePathWithParent);
	}

	return parentTree;
}

async function processProjects(projects: Projects, gitlabFetcher: GitlabFetcher, gitlabBranches: GitlabBranches, envProvider: EnvironmentProvider): Promise<void> {
	const requestedDocumentationBranchName = envProvider.getDOCUMENTATION_BRANCH_NAME_SOURCE();
	for (const project of projects) {
		logger.info(`Processing project: ${project.name}`);
		let branchName = requestedDocumentationBranchName;

		if (requestedDocumentationBranchName != project.default_branch && !await gitlabBranches.branchExists(project.id, requestedDocumentationBranchName)) {
			logger.debug(`Requested branch '${requestedDocumentationBranchName}' was not found! Using default branch '${project.default_branch}'.`);
			branchName = project.default_branch;
		}

		logger.info('Fetching pom.xml...');
		const pomInformation = await gitlabFetcher.fetchPom(project.id, branchName);
		if (!pomInformation) {
			logger.warn(`The project with id: ${project.id} does not contain pom.xml file!`);
			continue;
		}
		const { pom } = readPOM(pomInformation);
		if (pom.project.packaging === 'pom') {
			let submodules = pom.project.modules?.module || [];
			if (!Array.isArray(submodules)) {
				submodules = [submodules];
			}

			if (!fs.existsSync(`runtime/${project.id}`)) {
				await fs.promises.mkdir(`runtime/${project.id}`);
			}

			const submodulesTree = await processSubmodulesRecursive(project, branchName, submodules, gitlabFetcher);
			if (Object.keys(submodulesTree).length > 0) {
				await fs.promises.writeFile(`runtime/${project.id}_tree.json`, JSON.stringify(submodulesTree), { encoding: 'utf-8', flag: 'w' });
			}
		}

		logger.info('Fetching changes...');
		const changes = await gitlabFetcher.fetchChanges(project.id, branchName, envProvider.getCHANGES_FILE_NAME() || 'changes.json');
		if (!changes) {
			logger.verbose('Changes does not exist yet.');
		}

		await fetchDocs(project, branchName, gitlabFetcher);
	}
}

async function main() {
	const envProvider: EnvironmentProvider = new EnvironmentProvider();
	const httpClient: HttpsClient = new HttpsClient(envProvider);
	const gitlabFetcher: GitlabFetcher = getGitlabFetcherInstance(envProvider, httpClient);
	const gitlabBranches: GitlabBranches = getGitlabBranchesInstance(envProvider, httpClient);

	try {
		await fs.promises.mkdir('./runtime', { recursive: true });
		logger.info(`Created runtime directory... ${path.resolve('./runtime')}`);
	} catch (err) {
		logger.error('Could not create runtime directory!', err);
		return;
	}

	const mainGroup = await gitlabFetcher.fetchMainGroup();
	if (!mainGroup) {
		logger.error('Could not obtain main group of the project...');
		return;
	}
	logger.info(`Main group id: ${mainGroup.id}, name: '${mainGroup.name}'.`);

	const projects = await gitlabFetcher.fetchProjects(mainGroup);
	if (!projects) {
		logger.error('Could not obtain projects in main group...')
		return;
	}
	logger.info(`Loaded #${projects.length} projects.`);

	await processProjects(projects, gitlabFetcher, gitlabBranches, envProvider);
}

main().catch((reason: Error) => {
	logger.error(reason.message);
	logger.error(reason.stack);
})
