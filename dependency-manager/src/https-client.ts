import * as https from 'https';


import { EnvironmentProvider } from './environment-provider';
import { getLogger, Logger } from './logger';


export class HttpsClient {

	static readonly GL_URL: string = 'https://gitlab.com/api/v4/';

	private readonly logger: Logger;
	private readonly privateToken: string;

	constructor(protected readonly envProvider: EnvironmentProvider) {
		this.logger = getLogger(HttpsClient.name);
		this.privateToken = envProvider.getCUSTOM_TOKEN();
	}

	async get<T>(urlPart: string): Promise<T | false> {
		const url = HttpsClient.GL_URL + urlPart;
		this.logger.info(`GET: ${url}`);
		return new Promise((resolve, reject) => {
			let data = '';
			const request = https.get(url, {
				headers: {
					'PRIVATE-TOKEN': this.privateToken
				}
			}, response => {
				response.setEncoding('utf8');

				response.on('data', (chunk) => {
					data += chunk;
				});

				// The whole response has been received. Print out the result.
				response.on('end', () => {
					if (response.statusCode === 200) {
						resolve(JSON.parse(data));
					} else {
						this.logger.warn(data);
						this.logger.warn(response.statusCode);
						this.logger.warn(response.statusMessage);
						resolve(false);
					}
				});
			});

			// Log errors if any occur
			request.on('error', (error) => {
				reject(error);
			});

			// End the request
			request.end();
		});
	}

	async post<P, T>(urlPart: string, payload?: P): Promise<T | false> {
		const url = HttpsClient.GL_URL + urlPart;
		this.logger.info(`POST: ${url}`);
		return new Promise((resolve, reject) => {
			let data = '';
			const request = https.request(url, {
				method: 'POST',
				headers: {
					'PRIVATE-TOKEN': this.privateToken,
					'Content-Type': 'application/json'
				}
			}, response => {
				response.setEncoding('utf8');

				response.on('data', (chunk) => {
					data += chunk;
				});

				// The whole response has been received. Print out the result.
				response.on('end', () => {
					if (`${response.statusCode}`.startsWith('2')) {
						resolve(JSON.parse(data));
					} else {
						this.logger.warn(data);
						this.logger.warn(response.statusCode);
						this.logger.warn(response.statusMessage);
						this.logger.warn(response.rawHeaders.join(', '));
						resolve(false);
					}
				});
			});

			// Log errors if any occur
			request.on('error', (error) => {
				reject(error);
			});

			if (payload) {
				request.write(JSON.stringify(payload));
			}

			// End the request
			request.end();
		});
	}

	async put<P, T>(urlPart: string, payload?: P): Promise<T | false> {
		const url = HttpsClient.GL_URL + urlPart;
		this.logger.info(`PUT: ${url}`);
		return new Promise((resolve, reject) => {
			let data = '';
			const request = https.request(url, {
				method: 'PUT',
				headers: {
					'PRIVATE-TOKEN': this.privateToken,
					'Content-Type': 'application/json'
				}
			}, response => {
				response.setEncoding('utf8');

				response.on('data', (chunk) => {
					data += chunk;
				});

				// The whole response has been received. Print out the result.
				response.on('end', () => {
					if (response.statusCode === 200) {
						resolve(JSON.parse(data));
					} else {
						this.logger.warn(data);
						this.logger.warn(response.statusCode);
						this.logger.warn(response.statusMessage);
						this.logger.warn(response.rawHeaders.join(', '));
						resolve(false);
					}
				});
			});

			// Log errors if any occur
			request.on('error', (error) => {
				reject(error);
			});

			if (payload) {
				request.write(JSON.stringify(payload));
			}

			// End the request
			request.end();
		});
	}
}
