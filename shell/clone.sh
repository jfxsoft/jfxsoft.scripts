#!/bin/bash

# Check if GROUP_ID and GITLAB_TOKEN are provided as a parameter
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <GROUP_ID> <GITLAB_TOKEN>"
    exit 1
fi

# Assign the first parameter to GROUP_ID
GROUP_ID=$1
# Assign the second parameter to GITLAB_TOKEN
GITLAB_TOKEN=$2
# Define gitlab URL
GITLAB_API_URL="https://gitlab.com/api/v4"

# Function to clone repositories and rename directories
clone_repos() {
    local group_id=$1
    local repos=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API_URL/groups/$group_id/projects?per_page=100" | jq -r '.[].ssh_url_to_repo')

    for repo in $repos; do
        # Extract the project name from the repo URL
        local project_name=$(basename -s .git "$repo")

        # Clone the repository
        git clone "$repo"

        # Change to the project directory
        cd "$project_name" || continue

        # Check if pom.xml exists and extract artifactId
        if [ -f "pom.xml" ]; then
            artifact_id=$(xmllint --xpath "//*[local-name()='project']/*[local-name()='artifactId']/text()" pom.xml)

            # Move back to the parent directory
            cd ..

            # Rename the directory only if project_name and artifact_id are different
            if [ "$project_name" != "$artifact_id" ]; then
                mv "$project_name" "$artifact_id"
            fi
        else
            # Move back to the parent directory if pom.xml does not exist
            cd ..
        fi
    done
}

# Function to process groups and subgroups
process_group() {
    local group_id=$1
    echo "Processing group ID: $group_id"

    # Clone repositories in the current group
    clone_repos "$group_id"

    # Get subgroups of the current group
    local subgroups=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API_URL/groups/$group_id/subgroups?per_page=100" | jq -r '.[].id')

    # Recursively process each subgroup
    for subgroup in $subgroups; do
        process_group "$subgroup"
    done
}

# Start processing from the top-level group
process_group "$GROUP_ID"