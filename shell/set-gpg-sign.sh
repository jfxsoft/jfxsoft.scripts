#!/bin/bash

for dir in */ ; do
    if [ -d "$dir/.git" ]; then
        echo "Setting GPG sign in $dir"
        cd "$dir"
        git config commit.gpgsign true
        cd ..
    else
        echo "$dir is not a git repository."
    fi
done