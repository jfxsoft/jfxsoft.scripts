#!/bin/bash

# Loop through each subdirectory
for dir in */ ; do
    if [ -d "$dir/.git" ]; then
        echo "Pulling changes in dir: $dir"
        cd "$dir"
        git pull
        cd ..
    else
        echo "$dir is not a git repository."
    fi
done
