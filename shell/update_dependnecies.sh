#!/bin/bash

# Check if PROJECT_ID and GITLAB_TOKEN are provided as a parameter
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <PROJECT_ID> <GITLAB_TOKEN>"
    exit 1
fi

# Assign the first parameter to ID
PROJECT_ID=$1
# Assign the second parameter to GITLAB_TOKEN
PRIVATE_TOKEN=$2
# GitLab API details
GITLAB_API_URL="https://gitlab.com/api/v4"
# Get the current branch name
BRANCH_NAME=$CI_COMMIT_BRANCH

# Check if branch name is provided
if [ -z "$BRANCH_NAME" ]; then
    echo "No branch name provided."
    exit 1
fi

# Exit if the branch name is "main" or "develop"
if [ "$BRANCH_NAME" == "main" ] || [ "$BRANCH_NAME" == "develop" ]; then
    echo "Branch name is 'main' or 'develop'. Exiting script."
    exit 0
fi

echo "Current branch: $BRANCH_NAME"
echo "Project ID: $PROJECT_ID"

# Function to get the repository ID from the repository name
get_repo_id() {
    local repo_name=$1
    local response
    local repo_id

    # Check if input parameters are not empty
    if [ -z "$repo_name" ]; then
        echo "Repository name was not provided!"
        return 1
    fi

    response=$(curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$GITLAB_API_URL/projects?search=$repo_name")
    repo_id=$(echo "$response" | jq -r '.[0].id')

    echo "$repo_id"
}

# Function to check if a branch exists in the GitLab repository
check_branch_exists() {
    local repo_id=$1
    local branch=$2
    local response
    local branchEncoded

    # Check if input parameters are not empty
    if [ -z "$repo_id" ] || [ -z "$branch" ]; then
        echo "Both repo_id and branch must be provided."
        return 1
    fi

    branchEncoded=$(curl -Gso /dev/null -w %{url_effective} --data-urlencode "$branch" "http://example.com" | cut -d'?' -f2-)
    echo "Checking branch '$branch' existence in a repo: '$repo_id'"
    response=$(curl -Gs --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$GITLAB_API_URL/projects/$repo_id/repository/branches/$branchEncoded")
    receivedName=$(echo "$response" | jq -r '.name')

    if [ "$receivedName" == "$branch" ]; then
        return 0
    else
        return 1
    fi
}

# Function to get the current branch name
get_current_branch_name() {
    local BRANCH_NAME

    if [ -n "$CI_COMMIT_BRANCH" ]; then
        BRANCH_NAME="$CI_COMMIT_BRANCH"
        log_info "Branch name found in 'CI_COMMIT_BRANCH'."
    elif [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" ]; then
        BRANCH_NAME="$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
        log_info "Branch name found in 'CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'."
    else
        echo "No branch name found. There will be no dependency version switching."
        return 1
    fi

    echo "$BRANCH_NAME"
}

# Retrieve a list of repositories defined in the project's pom.xml file
REPOSITORIES=$(xmllint --xpath '//*[local-name()="repositories"]/*[local-name()="repository"]/*[local-name()="id"]/text()' pom.xml)

# Modify repository IDs by removing ".parent" suffix if present
declare -A modified_repos
for repo in $REPOSITORIES; do
    modified_repo=$(echo "$repo" | sed 's/\.parent$//')
    modified_repos["$modified_repo"]="$modified_repo"
    echo "Found a repository: $modified_repo"
done

# Check all properties in the pom.xml file,
DEPENDENCIES=$(xmllint --xpath '//*[local-name()="project"]/*[local-name()="properties"]/*' pom.xml | \
# pair the name and value together,
sed -E 's/<([^>]+)>([^<]*)<\/\1>/\1=\2/' | \
# replace word "project" with "jfxsoft" and "version" with "parent"
sed -E 's/^project/jfxsoft/g; s/version/parent/g' | \
# filter out only key paris starting with "jfxsoft"
grep '^jfxsoft')

echo "Found dependencies with prefix 'jfxsoft':"

# Group dependencies by modified repository ID using prefix comparison
declare -A dependency_groups
declare -A dependency_versions
while IFS='=' read -r key value; do
    ARTIFACT_ID=$(echo "$key" | xargs)
    CURRENT_VERSION=$(echo "$value" | xargs)
    echo "Found dependency: $ARTIFACT_ID with version: $CURRENT_VERSION"

    # Find the corresponding modified repository ID using prefix comparison
    best_match=""
    for repo in "${!modified_repos[@]}"; do
        if [[ "$ARTIFACT_ID" == "$repo"* ]]; then
            best_match=$repo
            break
        fi
    done

    if [ -n "$best_match" ]; then
        modified_repo=${modified_repos["$best_match"]}
        if [ -z "${dependency_groups[$modified_repo]}" ]; then
            dependency_groups["$modified_repo"]="$ARTIFACT_ID"
            dependency_versions["$modified_repo"]="$CURRENT_VERSION"
        else
            dependency_groups["$modified_repo"]+=$'\n'"$ARTIFACT_ID"
        fi
    else
      echo "There is no match for: '$ARTIFACT_ID'"
    fi
done <<< "$DEPENDENCIES"

echo "Dependencies sorted into groups..."

# Process each dependency group
for group in "${!dependency_groups[@]}"; do
    echo "Processing group: $group"

    # Get the repository ID for the group
    repo_id=$(get_repo_id "$group")

    # Check if the branch exists for the group
    if check_branch_exists "$repo_id" "$BRANCH_NAME"; then
        echo "Branch $BRANCH_NAME exists for repo $repo_id with group $group."

        CURRENT_VERSION="${dependency_versions[$group]}"
        NEW_VERSION="${CURRENT_VERSION}-${BRANCH_NAME}"
        # Replace "jfxsoft" with "project" in the group variable
        group=$(echo "$group" | sed 's/^jfxsoft/project/')
        # mvn versions:set-property --quiet -Dproperty="$group.version" -DnewVersion="$NEW_VERSION"
        echo "Updated property '$group.version' from $CURRENT_VERSION to version $NEW_VERSION"
    else
        echo "Branch $BRANCH_NAME does not exist for group $group."
    fi
done
